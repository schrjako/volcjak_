<head>

	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">

	<title> Volčjak - <?php print($name);?> </title>

	<link rel="shortcut icon" href="./favicon.ico?v=<?php echo filemtime("./favicon.ico"); ?>" />
	<link rel="stylesheet" type="text/css" href="style.css?<?php echo filemtime("style.css"); ?>" />

	<meta name="viewport" content="width=device-width, initial-scale=1" />
	
	<script type='text/javascript' src='js/constants.js?<?php echo filemtime("js/constants.js"); ?>' ></script>
	<script type='text/javascript' src='js/utils.js?<?php echo filemtime("js/utils.js"); ?>' ></script>
	<script type='text/javascript' src='js/Cell.js?<?php echo filemtime("js/Cell.js"); ?>' ></script>
	<script type='text/javascript' src='js/Row.js?<?php echo filemtime("js/Row.js"); ?>' ></script>
	<script type='text/javascript' src='js/Edit.js?<?php echo filemtime("js/Edit.js"); ?>' ></script>
	<script type='text/javascript' src='js/Table.js?<?php echo filemtime("js/Table.js"); ?>' ></script>

</head>
