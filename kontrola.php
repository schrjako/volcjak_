<?php

require_once("./db/connect.php");
require_once("./db/Kontrola_mult.php");

$name = "Kontrola";

if(!array_key_exists("key", $_GET)){
	header("HTTP/1.0 400 Bad Request");
	exit("Error: missing key!<br>\n");
}

if(!array_key_exists("viewkey", $_GET)){
	header("HTTP/1.0 400 Bad Request");
	exit("Error: missing viewkey!<br>\n");
}

$kontrola = new Kontrola_mult($_GET['key'], $_GET['viewkey']);

if(isset($_GET['KT'])) $kontrola->select("KT", $_GET['KT']);
if(isset($_GET['proga'])) $kontrola->select("proga", $_GET['proga']);

?>
<!DOCTYPE html>
<html>
<?php
	include_once("head.php");
?>
	<body id="body">
		<h1>
<?php
if(isset($kontrola->sel_KT) && isset($kontrola->sel_proga)){
	print("\t\t\t<a href='./kontrola.php?key=" . $kontrola->get_key() . "&viewkey=" . $kontrola->get_key(view: true) .
		"&KT=" . $kontrola->sel_KT . "&proga=" . $kontrola->sel_proga . "'>\n\t");
}
print("\t\t\t$name " . $kontrola->get_name() . "\n");
if(isset($kontrola->sel_KT) && isset($kontrola->sel_proga)){
	print("\t\t\t</a>\n");
}
?>
		</h1>
		<script type='text/javascript'>
<?php
print("\t\t\tlet Table_objs = {}\n");
print($kontrola->get_Table_def(indent: "\t\t\t"));
?>
		</script>
<?php print($kontrola->get_links(indent: "\t\t"));?>
<?php
if($kontrola->key->t_name == "tekmovanje"){
	print("\t\t<p style='text-align: right'><a href='./?key=" . $kontrola->key->get_key() . "'>nazaj na index</a></p>\n");
}
?>
	</body>
</html>
