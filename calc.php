<?php

require_once("./db/connect.php");
require_once("./db/tables.php");

$name = "Računanje";

$t_name = "proga";

$table = &$tables[$t_name];
$table->set_key($_GET['key']);

if(!isset($table->key) || $table->key->valid == false){
	header("HTTP/1.0 400 Bad Request");
	exit("The key {$_POST['key']} doesn't seem to be valid, check the correctness of your link!<br>\n");
}

if($table->key->t_name != "tekmovanje" &&
		$table->key->t_name != "kategorija" &&
		$table->key->t_name != "proga"){
	header("HTTP/1.0 400 Bad Request");
	exit("The key {$_POST['key']} is for table {$table->key->t_name}, but should be tekmovanje, kategorija, or proga!<br>\n");
}

?>
<!DOCTYPE html>
<html>
<?php
	include_once("head.php");
?>
	<body id="body">
		<h1> <?php print($t_name);?> </h1>
<?php
print($table->get_calc_buts(indent: "\t\t"));
?>
<?php
if($table->key->t_name == "tekmovanje"){
	print("\t\t<p style='text-align: right'><a href='./?key=" . $table->key->get_key() . "'>nazaj na index</a></p>\n");
}
?>
	</body>
</html>
