<?php

require_once("./db/connect.php");
require_once("./db/Key.php");
require_once("./db/tables.php");

/*
 * Creates th and td cells for given proga.
 */
class Proga{

	/*
	 * id and ime of this proga.
	 */
	public function __construct($id, $ime){
		$this->id = $id;
		$this->ime = $ime;
	}

	/*
	 * Adds column rows to $titlerow according to the flags set with $_GET.
	 */
	public function add_th_cells(&$titlerow){
		global $_GET;
		if(isset($_GET['sum'])) return;
		if(isset($_GET['proga_sum'])) $titlerow[] = "vsota " . $this->ime;
		else{
			$titlerow[] = "časovnica " . $this->ime;
			global $link;
			if(isset($_GET['KT_narazen'])){
				$q = "SELECT naloga.ime FROM naloga WHERE (naloga.id_proga = $this->id AND naloga.type = 'KT') ORDER BY naloga.id;\n";
				$KTs = mysqli_query($link, $q);
				while($KT = mysqli_fetch_object($KTs)){
					$titlerow[] = $KT->ime;
				}
			}
			if(!isset($_GET['naloge_skp'])){
				$q = "SELECT naloga.ime FROM naloga WHERE (naloga.id_proga = $this->id AND " .
					"(naloga.type = 'naloga' OR naloga.type = 'hitrostna_cilj')) ORDER BY naloga.id;\n";
				$naloge = mysqli_query($link, $q);
				while($naloga = mysqli_fetch_object($naloge)){
					$titlerow[] = $naloga->ime;
				}
			}
		}
	}

	/*
	 * Adds tocke to $row for this proga and ekipa with id $id_ekipa.
	 */
	public function add_tocke(&$row, $id_ekipa){
		global $link, $_GET;
		if(isset($_GET['sum'])) return;
		if(isset($_GET['proga_sum'])){
			$q = "SELECT SUM(rezultat.tocke) AS sum FROM rezultat JOIN naloga ON rezultat.id_naloga = naloga.id " .
				"WHERE naloga.id_proga = $this->id AND rezultat.id_ekipa = $id_ekipa;\n";
			$ans = mysqli_query($link, $q);
			$sum = mysqli_fetch_object($ans);
			$row[] = $sum->sum;
		}
		else{
			$qrez = "((SELECT rezultat.* FROM rezultat WHERE rezultat.id_ekipa = $id_ekipa) AS rez)";
			$q = "SELECT rez.* FROM naloga LEFT JOIN $qrez ON rez.id_naloga = naloga.id " .
				"WHERE naloga.id_proga = $this->id AND naloga.type = 'cilj';\n";
			$casovnica = mysqli_query($link, $q);
			$casovnica = mysqli_fetch_object($casovnica);
			$row[] = $casovnica->tocke;
			if(isset($_GET['KT_narazen'])){
				$q = "SELECT rez.tocke FROM naloga LEFT JOIN $qrez ON rez.id_naloga = naloga.id " .
					"WHERE (naloga.id_proga = $this->id AND naloga.type = 'KT') ORDER BY naloga.id;\n";
				$tocke = mysqli_query($link, $q);
				while($KT = mysqli_fetch_object($tocke)){
					$row[] = $KT->tocke;
				}
			}
			if(!isset($_GET['naloge_skp'])){
				$q = "SELECT rez.tocke FROM naloga LEFT JOIN $qrez ON rez.id_naloga = naloga.id WHERE (naloga.id_proga = $this->id AND " .
					"(naloga.type = 'naloga' OR naloga.type = 'hitrostna_cilj')) ORDER BY naloga.id;\n";
				$naloge = mysqli_query($link, $q);
				while($naloga = mysqli_fetch_object($naloge)){
					$row[] = $naloga->tocke;
				}
			}
		}
	}

}

/*
 * Creates <table> for given viewkey.
 */
class Kategorija{

	/*
	 * Checks whether $viewkey is ok and gets all proga and ekipa for this kategorija.
	 */
	public function __construct($viewkey){
		$this->valid = false;
		$this->viewkey = new Key($viewkey, true);
		if(!$this->viewkey->valid) return;
		global $link;
		$q = "SELECT proga.id, proga.ime FROM proga JOIN (kategorija, tekmovanje)\n" .
			"\tON (proga.id_kat = kategorija.id AND kategorija.id_tekm = tekmovanje.id)\n" .
			"\tWHERE " . $this->viewkey->get_where($where) . ";";
		$proge = mysqli_query($link, $q);
		$this->proga = [];
		while($proga = mysqli_fetch_object($proge)){
			$this->proga[$proga->id] = new Proga($proga->id, $proga->ime);
		}
		$q = "SELECT ekipa.id, ekipa.stevilka, ekipa.ime FROM ekipa JOIN (kategorija, tekmovanje)\n" .
			"\tON (ekipa.id_kat = kategorija.id AND kategorija.id_tekm = tekmovanje.id)\n" .
			"\tWHERE " . $this->viewkey->get_where($where) . ";";
		$ekipe = mysqli_query($link, $q);
		$this->ekipa = [];
		while($ekipa = mysqli_fetch_object($ekipe)){
			$this->ekipa[$ekipa->id] = ["stevilka" => $ekipa->stevilka, "ime" => $ekipa->ime];
		}
		$this->valid = true;
	}

	/*
	 * Creates <table> for this kategorija.
	 *
	 * To change what the table shows add following words to $_GET:
	 *  - 'sum' - display only the grand total
	 *  - 'proga_sum' - add sum for each proga to grand total
	 *  - 'KT_narazen' - display each naloga of type KT in its own column, by default they're all in one column.
	 *  - 'naloge_skp' - display all naloga of type naloga in one column.
	 */
	public function get_table($indent = ""){
		global $link, $_GET;
		$titlerow = ["mesto", "številka", "ekipa", "vsota točk"];
		foreach($this->proga as $id_proga => $proga){
			$proga->add_th_cells($titlerow);
		}
		if(!isset($_GET['sum']) && !isset($_GET['proga_sum'])){
			if(!isset($_GET['KT_narazen'])){
				$titlerow[] = "KT-ji";
			}
			if(isset($_GET['naloge_skp'])){
				$titlerow[] = "naloge";
			}
		}
		$html_titlerow = "$indent\t<tr>\n" .
			"$indent\t\t<th>" . implode("</th>\n$indent\t\t<th>", $titlerow) . "</th>\n" .
			"$indent\t</tr>\n";
		$mesto = 0;
		$st = 0;
		$ptocke = -1;
		$q = "SELECT ekipa.id, ekipa.v_konkurenci, SUM(rezultat.tocke) AS sum FROM ekipa\n" .
			"\tJOIN (rezultat, naloga, proga, kategorija, tekmovanje)\n" .
			"\tON (rezultat.id_ekipa = ekipa.id AND rezultat.id_naloga = naloga.id AND naloga.id_proga = proga.id AND\n" .
			"\t\tekipa.id_kat = kategorija.id AND proga.id_kat = kategorija.id AND kategorija.id_tekm = tekmovanje.id)\n" .
			"\tWHERE " . $this->viewkey->get_where($where) . "\n" .
			"\tGROUP BY ekipa.id ORDER BY SUM(rezultat.tocke) DESC, ekipa.id ASC;\n";	//	TODO ORDER BY customizable
		$ekipe = mysqli_query($link, $q);
		while($ekipa = mysqli_fetch_object($ekipe)){
			$row = [];
			if($ekipa->v_konkurenci){
				++$st;
				if($ekipa->sum != $ptocke) $mesto = $st;
				$ptocke = $ekipa->sum;
			}
			else{
				$mesto = "diskval";
			}
			$row = array_merge(["$mesto."], $this->ekipa[$ekipa->id], [$ekipa->sum]);
			foreach($this->proga as $id_proga => $proga){
				$proga->add_tocke($row, $ekipa->id);
			}
			if(!isset($_GET['sum']) && !isset($_GET['proga_sum'])){
				if(!isset($_GET['KT_narazen'])){
					$q = "SELECT SUM(rezultat.tocke) AS sum FROM rezultat JOIN naloga ON rezultat.id_naloga = naloga.id " .
						"WHERE naloga.type = 'KT' AND rezultat.id_ekipa = $ekipa->id;\n";
					$KTs = mysqli_query($link, $q);
					$tocke = mysqli_fetch_object($KTs);
					$row[] = $tocke->sum;
				}
				if(isset($_GET['naloge_skp'])){
					$q = "SELECT SUM(rezultat.tocke) AS sum FROM rezultat JOIN naloga ON rezultat.id_naloga = naloga.id " .
						"WHERE naloga.type = 'naloga' AND rezultat.id_ekipa = $ekipa->id;\n";
					$naloge = mysqli_query($link, $q);
					$tocke = mysqli_fetch_object($naloge);
					$row[] = $tocke->sum;
				}
			}
			$html_rows .= "$indent\t<tr class='trow_" . (($st&1)?"odd":"even") . ( $ekipa->v_konkurenci ? "" : " disqualified" ) .
				"' name='{$this->ekipa[$ekipa->id]->stevilka}'>\n" .
					"$indent\t\t<td>" . implode("</td>\n$indent\t\t<td>", $row) . "</td>\n" .
				"$indent\t</tr>\n";
		}
		return "$indent<table>\n" .
			$html_titlerow .
			$html_rows .
			"$indent</table>\n";
	}

}

$name = "Rezultati";

$kategorija = new Kategorija($_GET['viewkey']);

if(!$kategorija->valid){
	header("HTTP/1.0 400 Bad Request");
	exit("Error creating kategorija!\n");
}

?>
<!DOCTYPE html>
<html>
<?php
	include_once("head.php");
?>
	<body id="body">
		<h1> Rezultati </h1>
<?php
if(isset($_GET['key'])){
	$table = $tables["proga"];
	$table->set_key($_GET['key']);
	if($table->key->valid)
		print($table->get_calc_buts(indent: "\t\t"));
}
print($kategorija->get_table(indent: "\t\t"));
?>
	</body>
</html>
