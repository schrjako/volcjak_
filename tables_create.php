<?php

/*
 * Creates tables with names in array $tables, calls on the create_definition found in the
 * file with name $tables + '.php'.
 */

require_once("./db/constants.php");
require_once("./db/connect.php");
require_once("./db/tables.php");

foreach($tables as $t_name => $table){

	$table_create = $table->get_table_create();
	$ans = mysqli_query($link, $table_create);

	if($ans === true)
		print("Successfully created table ".$t_name."<br>\n");
	else
		print("Failed creating table ".$t_name."<br>\n");
}

?>
