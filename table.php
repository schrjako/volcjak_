<?php

require_once("./db/connect.php");
require_once("./db/tables.php");

$name = $_GET['name'];

$t_name = $_GET['name'];

if(!array_key_exists("$t_name", $tables)){
	header("HTTP/1.0 400 Bad Request");
	exit("Error: table [$t_name] doesn't exist!<br>\n");
}

$table = &$tables[$t_name];
$table->set_key($_GET['key']);

if(!isset($table->key) || $table->key->valid == false){
	header("HTTP/1.0 400 Bad Request");
	exit("The key {$_POST['key']} doesn't seem to be valid, check the correctness of your link!<br>\n");
}

?>
<!DOCTYPE html>
<html>
<?php
	include_once("head.php");
?>
	<body id="body">
		<h1> <?php print($t_name);?> </h1>
		<script type='text/javascript'>
<?php
print("\t\t\tlet Table_obj = " . $table->get_Table_def(indent: "\t\t\t\t"));
?>
		</script>
		<div>
<?php
print($table->get_links(indent: "\t\t\t"));
?>
		</div>
<?php
if($table->key->t_name == "tekmovanje"){
	print("\t\t<p style='text-align: right'><a href='./?key=" . $table->key->get_key() . "'>nazaj na index</a></p>\n");
}
?>
	</body>
</html>
