<?php

include_once("db/utils.php");

/*
 * Transforms array of filters to MySQL syntax.
 */
function mysql_filters($filters){
	if(count($filters)==0) return "";
	$ret = " WHERE";
	$first = true;
	foreach($filters as $key => $value){
		if($first) $first = false;
		else $ret .= " AND";
		$ret .= " $key = '$value'";
	}
	return $ret;
}

/*
 * Fetches name (ime) of referenced row. Caches results to minimize db queries.
 */
function get_name($reference, $id){
	global $link;
	static $v = [];
	if($v[$reverence.$id]) return $v[$reference.$id];
	sscanf($reference, "%[^(](%[^)])", $name, $id_name);
	$query = mysqli_query($link, "SELECT ime FROM $name WHERE $id_name = '$id';");
	return $v[$reference.$id] = mysqli_fetch_object($query)->ime;
}

/*
 * Generates javascript object which describes the type of data that is
 * in a given column. The object has up to 3 members: name, type and restrict.
 *  - name defines the name of the column in db,
 *  - type defines the type of data (date, time, text, list, int, float, bool) and
 *      subsequently the member restrict,
 *  - restrict defines a restricting property on the given field:
 *  	~ text - maximum length of text
 *  	~ list - object (id: name) which defines options for FOREIGN KEY members
 *
 * DATETIME is split up in two cells - date and time, which are handled separately
 * on the frontend.
 */
function input_type($table, $id, $key, $type, &$c){
	if($type == "DATETIME"){
		++$c;
		$ret = "{'name':'$key', 'type':'date'},\n\t\t$c:{'name':'$key', 'type':'time'}";
		return $ret;
	}
	$restrict = "";
	if(strncmp($type, "VARCHAR", 7) == 0){
		list($char, $len) = fk_split($type);
		$restrict = ", 'restrict':'$len'";
		$type = "'text'";
	}
	else if(array_key_exists('f_keys', $table) && array_key_exists($key, $table['f_keys'])){
		global $link;
		list($ft_name, $fid_name) = fk_split($table['f_keys'][$key]);

		if(array_key_exists($id['name'], $table['f_keys'])){
			$id['name']='id';
		}

		$q = get_db_query($ft_name, $id);
		$ans = mysqli_query($link, $q);
		$type = "'list'";
		$restrict = "";
		while($row = mysqli_fetch_object($ans)){
			if($restrict == "") $restrict = ", 'restrict':{";
			else $restrict .= ", ";
			$restrict .= "{$row->id}:'{$row->ime}'";
		}
		$restrict .= '}';
	}
	else{
		$type = "'" . strtolower($type) . "'";
	}
	return "{'name':'$key', 'type':$type$restrict}";
}

/*
 * Returns the primary key values for given row.
 */
function get_primary_key($table, $row){
	$second = "";
	sscanf($table['p_key'], "%[^,], %s", $first, $second);
	$ret = $row->$first;
	if($second != "") $ret .= ", " . $row->$second;
	return $ret;
}

/*
 * Creates html table with description in db/$name.php and rows
 * from table $name with restrictions from $id.
 */
function html_table($name, $id, $table_id='table'){
	if(!include("db/$name.php")){
		die("TABLE $t_name DOESN'T EXIST<br>\n");
	}
	global $link;

	print("<!-- table -->\n");
	print("<table id='$table_id' class='spreadsheet'>\n");

	/*
	 * Creates the titlerow and edit_table js object.
	 */
	print("\t<tr id='titlerow'>\n");
	$edit_table = "";
	$f_keys_table = "";
	$c=0;
	foreach($table['members'] as $key => $value){
		if($key != $table['p_key']){
			print("\t\t<th");
			if($value == "DATETIME") print(" colspan='2'");
			print(">$key</th>\n");
			if(in_array($key, $table['perms'][$id['perms_name']])){
				if($edit_table == "") $edit_table = "var edit_table = {\n";
				else $edit_table .= ",\n";
				$edit_table .= "\t\t$c:";
				$edit_table .= input_type($table, $id, $key, $value, $c);
			}
			if(array_key_exists('f_keys', $table) && array_key_exists($key, $table['f_keys'])){
				if($f_keys_table == "") $f_keys_table = "var f_keys_table = {\n";
				else $f_keys_table .= ",\n";
				$f_keys_table .= "\t\t$c:";
				$f_keys_table .= input_type($table, $id, $key, $value, $c);
			}
			++$c;
		}
	}
	$edit_table .= ",\n\t\t$c: {name: 'toolbar_select', type: 'bool'}";
	$edit_table .= "\n\t};";
	if($f_keys_table != "")
		$f_keys_table .= "\n\t};";
	print("\t</tr>\n");

	/*
	 * Creates table's body.
	 */
	$select = get_db_query($name, $id);
	$query = mysqli_query($link, $select);
	$r = 1;	//	counts rows
	$changes = "var changes = [undefined";	//	holds the changes made to table until they are saved
	                                      	//	or discarded
	while($row = mysqli_fetch_object($query)){
		$c = 0;	//	counts columns
		print("\t<tr id='datarow$r' class='trow_" . ((1&$r)?"even":"odd") . "'>\n");
		$changes .= ", {p_key: '" . get_primary_key($table, $row) . "', changes: undefined, selected: false}";
		foreach($table['members'] as $key => $value){
			if($key == $table['p_key']) continue;	//	PRIMARY KEY cannot be changed
			print("\t\t<td id='{$r}_$c'");
			if(in_array($key, $table['perms'][$id['perms_name']]))	//	user is allowed to change column
				print(" ondblclick=\"edit_start($r, $c);\"");
			if($table['f_keys'][$key])	//	title holds the FOREIGN KEY value because the cell holds its name
				print(" title='{$row->$key}'");
			print(">");
			if($table['f_keys'][$key]){	//	gets the name of the row referenced with FOREIGN KEY
				print(get_name($table['f_keys'][$key], $row->$key));
			}
			else if($value == "DATETIME"){	//	splits DATETIME to two separate cells
				print(substr($row->$key, 0, 10) . "</td>\n");
				++$c;
				print("\t\t<td id='{$r}_$c'");
				if(in_array($key, $table['perms'][$id['perms_name']]))
					print(" ondblclick=\"edit_start($r, $c);\"");
				print(">");
				print(substr($row->$key, 11, 8) . "</td>\n");
			}
			else {
				print($row->$key);
			}
			print("</td>\n");
			++$c;
		}
		print("\t</tr>\n");
		++$r;
	}

	$changes .= "];";

	print("</table>\n");

	print("<input type='button' value='shrani podatke' onclick='save()'/>\n");
	print("<input type='button' value='ponastavi tabelo' ondblclick='cancel()'/>\n");

	if(in_array("NEW", $table['perms'][$id['perms_name']]))
		print("<input type='button' value='dodaj vrsto' onclick='add_row()'/>\n");
	if(in_array("DEL", $table['perms'][$id['perms_name']])){
		print("<input type='button' value='toggle toolbar' onclick='toolbar_toggle()'/>\n");
		print("<input type='button' value='rm selected' onclick='selected_rm()'/>\n");
	}

	print("<!-- /table -->\n");
?>
<!-- user- and table-specific javascript -->
<script type='text/javascript'>
	var rlen = <?php print($r);?>;
	var clen = <?php print($c);?>;
	var table_id = '<?php print($table_id);?>';
	var auth = ['<?php print($id['name']);?>', '<?php print($id['hash']);?>'];
	var t_name = '<?php print($name);?>';
	<?php print("$edit_table\n"); ?>
	<?php print("$f_keys_table\n"); ?>
	<?php print("$changes\n"); ?>
</script>
<!-- user- and table-specific javascript -->
<?php
}

/*
 * Generates links to children of key with the same key.
 */
function same_key_links($id, $t_name){
	if(!include("db/{$id['t_name']}.php")){
		die("TABLE $t_name DOESN'T EXIST<br>\n");
	}
	print("<a href='table.php?name={$id['t_name']}&key=id:{$id['hash']}'>{$id['t_name']}</a><br>\n");
	foreach($table['children'] as $child){
		print("<a href='table.php?name=$child&key={$table['f_name']}:{$id['hash']}'>$child</a><br>\n");
	}
}

?>
