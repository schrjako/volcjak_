# Volčjak

Volčjak je sestem za računanje in reprezentacijo rezultatov na taborniških
tekmovanjih.

## Vsebina

V slovenskem delu Readme.md bo opis uporabe volčjaka, v upanju, da bo odgovoril
na vsa takovrstna vprašanja. Angleški del pa bo poleg tega vseboval tudi bolj
natančno tehnično razlago.

# Volcjak

Is a system for calculating and representing results of scouts' competitions.

## Contents

The english part of Readme.md will contain a user guide of volcjak, as well as
a more in-depth explanations of the inner workings. The latter will be
awailable only in english.

### Database

Volcjak makes use of a MySQL database (henceforth db). It connects to the db
with the login credentials given in db/config.php. You should specify variables
$server (db serveradress), $username, $password and $dbname.

#### Tables

tables_create.php creates all db tables. They are described in their respective
files in db/*tablename*.php as the array $table.

1. **name** denotes the name of the table.

2. **members** is an array of the table's members/columns with their respective
datatypes.

3. **p_key** defines the PRIMARY KEY of the table.

4. [**f_keys**] is an array of FOREIGN KEYs with their columnname and
*tablename(keycolumn)*.

5. [**perms**] describes the permissions of each table. Each member of the array
consists of a *key* and *value*. The *key* can be one of three options:

	1. *parent* - *value* is the name of a table, which gets access to all
			privileges in the current table.

	2. *admin* - *value* is the column name which has admin powers over the table.

	3. *membername* - *value* defines which permissions the user has when in
			possessionc of *membername*'s hash. *Value* is an array of permissions:

		- *membername* permits modification of the column *membername*,
		
		- *NEW* permits inserting new rows,
		
		- *DEL* permits deleting rows,
		
		- *HASH* permits obtaining the hash of specific rows,
		
		- *VIEW* permits viewing the specific row.
