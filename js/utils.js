/*
 * Abbreviation for document.getElementById
 */
function $(id) {
	return document.getElementById(id);
}

/*
 * Generates html id from rid and cid.
 */
function mkid(rid, cid){
	return rid + '_' + cid;
}

function create_button({value, onclick = undefined, ondblclick = undefined, append_to = "body"}={}){
	let button = document.createElement("input");
	button.type = "button";
	button.value = value;
	button.onclick = onclick;
	button.ondblclick = ondblclick;
	$(append_to).appendChild(button);
	return button;
}

/*
 * Creates Ajax object, sets its request type (GET/POST, POST by default), and its target
 * file.
 */
function get_ajax_object(srvr, req_type = "POST"){
	let req_obj = new XMLHttpRequest();
	req_obj.open(req_type, srvr);
	return req_obj;
}

/*
 * Sends a POST request to ./db/calc.php that calculates results.
 */
function calc(){
	let key = this.key;
	let table = this.table;
	let id = this.id;
	console.log("calculating", key, table, id);
	let data = new FormData();
	data.append("key", key);
	data.append("table", table);
	data.append("id", id);
	let obj = get_ajax_object("./db/calc.php");
	obj.onload = function (evt){
		console.log(this.status);
		console.log(this.response);
		console.log(key, table, id);
	};
	obj.send(data);
}
