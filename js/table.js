/*
 * Abbreviation for document.getElementById
 */
function $(id) {
	return document.getElementById(id);
}

/*
 * Generates html id from rid and cid.
 */
function mkid(rid, cid){
	return rid + '_' + cid;
}

/*
 * Depending on edit_table returns the right <input> html for editing old_value
 * in given column (cid).
 */
function input_type(cid){
	if(f_keys_table[cid] != undefined){
		ret = "<select>\n";
		for(var i in f_keys_table[cid].restrict){
			ret += "<option value='" + i + "'" +
				"> " + f_keys_table[cid].restrict[i] + " </option>\n";
		}
		ret += "</select>\n";
		return ret;
	}
	ret = "<input";
	switch(edit_table[cid].type){
		case 'text':
			ret += " type='text' maxlength='" + edit_table[cid].restrict + "'";
			break;
		case 'bool':
			ret += " type='checkbox'";
			break;
		case 'date':
			ret += " type='date'";
			break;
		case 'time':
			ret += " type='time' step='1'";
			break;
		case 'int':
			ret += " type='number'";
			break;
		case 'float':
			ret += " type='number' step='0.01'";
			break;
		case 'list':
			ret = "<select id='curr_edit'>\n";
			for(var i in edit_table[cid].restrict){
				ret += "<option value='" + i + "'" +
					"> " + edit_table[cid].restrict[i] + " </option>\n";
			}
			ret += "</select>\n";
			return ret;
	}
	ret += "/>\n";
	return ret;
}

/*
 * If the user has the neccessary permissions it changes the value of the table cell to
 * an <input> element to let him edit said value.
 */
function edit_start(rid, cid){
	if(edit_table[cid] == undefined || rid>=rlen || cid>=clen) return;
	var id = mkid(rid, cid);
	var cell = $(id);
	var old_value;	//	the value in db when last got or updated
	var curr_value;	//	value in the cell when editing started
	console.log(cell, id);
	if(edit_table[cid].type == 'list')
		curr_value = cell.title;
	else
		curr_value = cell.innerHTML;
	if(changes[rid].changes != undefined && changes[rid].changes[cid] != undefined)
		old_value = changes[rid].changes[cid][1];
	else
		old_value = curr_value;
	//	if the cell is already being edited
	if(cell.firstChild != null && cell.firstChild.id == 'curr_edit') return;
	console.log("edit start", cid, rid);
	cell.innerHTML = input_type(cid, old_value);
	console.log(cell, cell == undefined);
	var input = cell.firstChild;
	input.id = 'curr_edit';
	input.old_value = old_value;	//	set <input>'s old_value to the original value of the cell
	function get_edit_end(cid, rid){
		return (event)=>{edit_end(cid, rid)};
	}
	input.onblur = get_edit_end(cid, rid);	//	add event handler for losing focus
	if(edit_table[cid].type == 'bool') input.checked = curr_value == '1';
	else input.value = curr_value;	//	set <input>'s value to what was in the cell
	input.focus();	//	give focus to <input> element
}

/*
 * Returns event handler for starting edit.
 */
function get_edit_start(rid, cid){
	return (event)=>{edit_start(rid, cid)};
}

/*
 * Returns value of input_obj in an array. If the type is bool it returns the text
 * corresponding with the set value, in lists it returns the name and value of the
 * selected element and otherwise the value.
 */
function get_value(cid, input_obj){
	switch(edit_table[cid].type){
		case 'bool':
			if(input_obj.checked) return ['1'];
			return ['0'];
			break;
		case 'list':
			return [input_obj[input_obj.selectedIndex].innerText, 
							input_obj[input_obj.selectedIndex].value];
		default:
			return [input_obj.value];
	}
}

/*
 * Finishes the editing process and writes the value in <input> back to the table cell.
 */
function edit_end(cid, rid){
	var curr_edit = $('curr_edit');
	var cell = curr_edit.parentNode;
	var newvalue = get_value(cid, curr_edit);	//	value in <input>
	console.log('edit end', cid, rid);
	cell.innerHTML = newvalue[0];
	if(newvalue[newvalue.length-1] == curr_edit.old_value){
		//	deletes saved changes if the value was changed to its original form
		if(changes[rid].changes == undefined) return;
		delete changes[rid].changes[cid];
		if(Object.keys(changes[rid].changes).length == 0)
			changes[rid].changes = undefined;
		return;
	}
	if(newvalue[1] != undefined)
		cell.title = newvalue[1];	//	update FOREIGN KEY value
	if(changes[rid].changes == undefined)
		changes[rid].changes = {};	//	create empty object for changes to row rid
	if(changes[rid].changes[cid] == undefined)	//	first change at (rid, cid)
		changes[rid].changes[cid] = curr_edit.old_value;
}

/*
 * Returns an object for ajax requests.
 */
function get_ajax_object(){
	return new XMLHttpRequest();
}

/*
 * Sends user's changes to db.
 */
function save(){

	/*
	 * Returns function to be executed on return of sending data to the server.
	 * Makes use of the function's scope to create the same functions with different
	 * variables which define their behaviour.
	 *
	 * On success clears the changes from local memory as they are now permanent in the db
	 * as well.
	 */
	function get_onload_mod(rid){
		return function(evt){
			if(this.status == 200){
				console.log(this.response);
				changes[rid].changes = undefined;
			}
			else{
				console.log("error");
				console.log(this.status);
				console.log(this.response);
			}
		};
	}

	/*
	 * Similar to get_onload_mod but also sets the p_key (PRIMARY KEY) attribute in changes
	 */
	function get_onload_add(rid){
		return function(evt){
			if(this.status == 200){
				console.log("new id:", this.response);
				changes[rid].changes = undefined;
				changes[rid].p_key = this.response;
				for(var cid in f_keys_table){
					var cell = $(mkid(rid, cid));
					cell.title = cell.firstChild.value;
					cell.innerHTML = cell.firstChild.children[cell.firstChild.selectedIndex].innerHTML;
				}
				for(var cid in edit_table){
					if(edit_table[cid].name == 'toolbar_select') continue;
					var cell = $(mkid(rid, cid));
					cell.ondblclick = get_edit_start(rid, cid);
				}
			}
			else{
				console.log("error");
				console.log("status:", this.status);
				console.log(this.response);
			}
		}
	}

	console.log('saving');
	var request_arr = [];
	for(var rid = 1;rid<rlen;++rid){
		if(changes[rid].changes == undefined) continue;
		request_arr[rid] = get_ajax_object();
		if(changes[rid].p_key != -1){	//	Row is not new, modify it.
			request_arr[rid].open("POST", "db/row_mod.php");
			request_arr[rid].onload = get_onload_mod(rid);
			var data = new FormData();
			data.append('t_name', t_name);	//	table name
			data.append('p_key', changes[rid].p_key);	//	primary key of row to be modified
			data.append('key', auth[0] + ':' + auth[1]);	//	key hash defining permissions
			for(var cid in changes[rid].changes){	//	appends changes to be sent to the backend
				if(edit_table[+cid-1] != undefined && edit_table[+cid-1].name == edit_table[cid].name){
					data.append(edit_table[cid].name, $(mkid(rid, +cid-1)).innerHTML +
						' ' + $(mkid(rid, cid)).innerHTML);
				}
				else if(edit_table[+cid+1] !== undefined &&
					edit_table[+cid+1].name == edit_table[cid].name){
					data.append(edit_table[cid].name, $(mkid(rid, cid)).innerHTML +
						' ' + $(mkid(rid, +cid+1)).innerHTML);
				}
				else if(edit_table[cid].type == 'list'){
					data.append(edit_table[cid].name, $(mkid(rid, cid)).title);
				}
				else{
					data.append(edit_table[cid].name, $(mkid(rid, cid)).innerHTML);
				}
			}
			request_arr[rid].send(data);
		}
		else{	//	Row is new, add it.
			request_arr[rid].open("POST", "db/row_add.php");
			request_arr[rid].onload = get_onload_add(rid);
			var data = new FormData();
			data.append('t_name', t_name);	//	table name
			data.append('key', auth[0] + ':' + auth[1]);	//	key hash defining permissions
			for(var cid in changes[rid].changes){	//	appends changes to be sent to the backend
				if(f_keys_table[cid] != undefined){
					data.append(f_keys_table[cid].name, $(mkid(rid, cid)).firstChild.value);
				}
				else if(edit_table[cid] == undefined){
					continue;
				}else if(edit_table[+cid-1] != undefined && edit_table[+cid-1].name == edit_table[cid].name){
					data.append(edit_table[cid].name, $(mkid(rid, +cid-1)).innerHTML +
						' ' + $(mkid(rid, cid)).innerHTML);
				}
				else if(edit_table[+cid+1] != undefined &&
					edit_table[+cid+1].name == edit_table[cid].name){
					data.append(edit_table[cid].name, $(mkid(rid, cid)).innerHTML +
						' ' + $(mkid(rid, +cid+1)).innerHTML);
				}
				else if(edit_table[cid].type == 'list'){
					data.append(edit_table[cid].name, $(mkid(rid, cid)).title);
				}
				else{
					data.append(edit_table[cid].name, $(mkid(rid, cid)).innerHTML);
				}
			}
			request_arr[rid].send(data);
		}
	}
}

/*
 * Reverts the table back to its state when it was last saved or refreshed/opened.
 */
function cancel(){
	console.log('canceling');
	for(var rid = rlen; rid-- > 1; ){
		if(changes[rid].p_key != -1){
			for(var cid in changes[rid].changes){
				var id = mkid(rid, cid);
				if(edit_table[cid].type == 'list'){
					$(id).title = changes[rid].changes[cid];
					$(id).innerHTML = edit_table[cid].restrict[changes[rid].changes[cid]];
				}
				else $(id).innerHTML = changes[rid].changes[cid];
			}
			changes[rid].changes = undefined;
		}
		else{
			delete changes[rid];
			$(table_id).deleteRow(rid);
			--rlen;
		}
	}
}

/*
 * Adds select checkbox to end of row.
 */
function add_toolbar_select(rid){

	function get_onchange(rid, cid){
		return ()=>{
			changes[rid].selected = $(mkid(rid, cid)).firstChild.checked;
		}
	}

	var cell = $('datarow' + rid).insertCell(clen);
	cell.innerHTML = input_type(clen);
	cell.id = mkid(rid, clen);
	cell.firstChild.onchange = get_onchange(rid, clen);
	cell.firstChild.checked = changes[rid].selected;
}

/*
 * Adds a row to the table. FOREIGN KEYs can be decided upon until the first save
 * after that the row is convertet with the standard editing permissions.
 * p_key is set to -1 to denote a new row, after first save p_key is changed to
 * the right value.
 */
function add_row(){
	var row = $(table_id).insertRow(rlen);
	row.id = "datarow" + rlen;
	for(var cid = 0; cid < clen; ++cid){
		var cell = row.insertCell(cid);
		cell.id = mkid(rlen, cid);
		if(f_keys_table[cid] != undefined){
			cell.innerHTML = input_type(cid);
		}
		else{
			cell.ondblclick = get_edit_start(rlen, cid);
		}
	}
	changes[rlen] = {p_key: -1, changes: undefined, selected: false};
	for(var cid in f_keys_table){
		if(changes[rlen].changes == undefined) changes[rlen].changes = {};
		changes[rlen].changes[cid] = "";
	}
	if(v_toolbar) add_toolbar_select(rlen);
	console.log(row);
	++rlen;
}

var v_toolbar = false;

/*
 * Hides the toolbar.
 */
function toolbar_hide(){
	if(!v_toolbar) return;
	v_toolbar = false;

	$('titlerow').deleteCell(clen);
	for(var rid = 1; rid < rlen; ++rid)
		$('datarow' + rid).deleteCell(clen);
}

/*
 * Shows the toolbar
 */
function toolbar_show(){
	if(v_toolbar) return;
	v_toolbar = true;
	var cell = $('titlerow').insertCell(clen);
	cell.innerHTML = 'Označi';
	cell.id = mkid(0, clen);
	for(var rid = 1; rid < rlen; ++rid){
		add_toolbar_select(rid);
	}
}

/*
 * Toggles toolbar visibility.
 */
function toolbar_toggle(){
	if(v_toolbar) toolbar_hide();
	else toolbar_show();
}

/*
 * Removes selected rows.
 */
function selected_rm(){
	function get_onload_rm(rid){
		return function(evt){
			if(this.status == 200){
				console.log('success\n', this.response);
				if(rid<rlen){
					changes.splice(rid, 1);
					$(table_id).deleteRow(rid);
					--rlen;
				}
			}
			else if(this.status != undefined){
				console.log("error");
				console.log("status:", this.status);
				console.log(this.response);
			}
			while(rid --> 1) if(changes[rid].selected) break;
			if(rid < 1) return;
			var obj = get_ajax_object();
			obj.open("POST", "db/row_rm.php");
			obj.onload = get_onload_rm(rid);
			var data = new FormData();
			data.append('t_name', t_name);	//	table name
			data.append('p_key', changes[rid].p_key);	//	primary key of row to be modified
			data.append('key', auth[0] + ':' + auth[1]);	//	key hash defining permissions
			obj.send(data);
		}
	}
	get_onload_rm(rlen).bind({status: undefined})();
}
