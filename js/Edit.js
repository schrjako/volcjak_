class Editcol{
	name;	//	column name in db, DATETIME is split and gets prefixes d_ and t_ for date and time
	     	//	component respecitvely
	type;	//	data in column
	editable;	//	user allowed to edit
	options;	//	options for enum or FOREIGN KEY
	len;	//	max text length
	low;	//	min value
	high;	//	max value
	pars;	//	parents
	allsend;	//	always send - for DATETIME components which are in different cells but need the
	        	//	other in db
	link;	//	whether or not the cell is used for providing links

	default_value;	//	default value when creating new cell

	constructor({name, display_name, type, editable = false, options, len, low, high, allsend = false,
			link = false, default_value = undefined}={}){
		this.name = name;
		this.display_name = display_name;
		this.type = type;
		this.editable = editable;
		this.options = options;
		this.len = len;
		this.low = low;
		this.high = high;
		this.allsend = allsend;
		this.link = link;
		this.default_value = default_value;
	}

	set_parents(par){
		this.pars = par;
	}

	make_title_cell(){
		let cell = document.createElement("th");
		cell.innerHTML = this.display_name;
		cell.id = this.name;
		return cell;
	}

	/*
	 * Returns the default value for this cell, according to default_value.
	 * Anything that doesn't start with a colon is taken as a literal value.
	 * Starting with colon are commands:
	 *  - :date signifies the current date
	 *  - :time signifies the current time
	 * If default_value is not set (default), an empty string is returned.
	 */
	get_default_value(){
		console.log("getting def value", this.default_value);
		if(this.default_value == undefined) return "";
		if(this.default_value[0] != ':') return this.default_value;
		let dttm_string = new Date().toLocaleString("en", {year: "numeric", month: "2-digit", day: "2-digit",
			hour: "2-digit", minute: "2-digit", second: "2-digit",
			hourCycle: "h23", timeZone: "Europe/Ljubljana"});
		if(this.default_value == ":date"){
			let date_array = dttm_string.slice(0, 10).split("/");
			return date_array[2] + "-" + date_array[0] + "-" + date_array[1];
		}
		if(this.default_value == ":time"){
			return dttm_string.slice(12, 20);
		}
		return "";
	}
}

class Edit{
	cols;	//	Editcol objects describing the Table's columns
	par;	//	parent - Table object

	constructor(cols){
		this.cols = cols;
	}

	/*
	 * Sets parent (Table) for the Edit object and parents for its
	 * Editcol objects.
	 */
	set_parent(par){
		this.par = par;
		for(var col of this.cols){
			col.set_parents([this, par]);
		}
	}

	/*
	 * Gets Editcol object for given column.
	 */
	get_edit(cid){
		return this.cols[cid];
	}

	/*
	 * Creates title row.
	 */
	create_title_row(rid = "titlerow"){
		let row = document.createElement("tr");
		row.id = rid;
		for(var col of this.cols){
			row.appendChild(col.make_title_cell());
		}
		return row;
	}
}
