class Row{
	rid;	//	row id
	cells;	//	array of cells
	clen;	//	number of cells
	hash;	//	hash value for key of this row
	par;	//	parent (Table object)
	row;	//	reference to DOM row object
	p_key;	//	PRIMARY KEY
	selected;	//	bool denoting the selected status of the row

	constructor(rid, p_key, clen, cells, hash = ""){
		this.rid = rid;
		this.p_key = p_key;
		this.clen = clen;
		this.cells = cells;
		this.hash = hash;
		this.selected = false;
	}

	/*
	 * Sets parent for the row and for its cells.
	 */
	set_parent(par){
		this.par = par;
		for(let cell of this.cells){
			cell.set_parents([this, par]);
		}
	}

	/*
	 * Creates and returns HTML <table> row (<tr>).
	 */
	create_row({rid, id_prefix = "datarow", ghost_row=false}={}){
		this.row = document.createElement("tr");
		this.row.id = id_prefix + rid;
		for(var cid = 0; cid < this.clen; ++cid){
			this.row.appendChild(this.cells[cid].create_cell(ghost_row));
		}
		return this.row;
	}

	/*
	 * Sets innerHTML for all cells apropriately.
	 */
	set_inner_cell_HTML(){
		for(var cell of this.cells)
			cell.set_innerHTML();
	}

	/*
	 * On successful load of a mod request applies the changes
	 * that have been saved to db to the HTML <table>.
	 */
	get_onload_mod(row){
		return function (evt){
			console.log(this.status);
			console.log(this.response);
			if(this.status === 200){
				for(var cell of row.cells){
					cell.apply_change();
				}
			}
			else{
				alert("Error processing request [mod]\nHTML status code: " + this.status + "\nResponse:\n" + this.response + "\n");
			}
		}
	}

	/*
	 * On successful load of an add request sets the p_key, disables automatic editing on
	 * all columns, adds double-click editing to editable columns (by replacing all the cells)
	 * and adds the select-cell if select is on.
	 */
	get_onload_add(row){
		return function (evt){
			console.log(this.status);
			console.log(this.response);
			if(this.status === 200){
				[row.p_key, row.hash] = this.response.split(";");
				row.row.innerHTML = "";
				for(var cid = 0; cid < row.clen; ++cid){
					row.row.appendChild(row.cells[cid].create_cell());
					row.cells[cid].value_old = row.cells[cid].value_new;
					row.cells[cid].assign_mouse_touch_action();
				}
				if(row.par.select_on) row.show_select();
			}
			else{
				alert("Error processing request [add]\nHTML status code: " + this.status + "\nResponse:\n" + this.response + "\n");
			}
		}
	}

	/*
	 * Sends changes made to the row to the server.
	 */
	save(){
		var data = new FormData();
		data.append("action", (this.p_key == -1) ? "add" : "mod");
		data.append("t_name", this.par.tid);
		data.append("key", this.par.key);
		if(this.par.viewkey != undefined) data.append("viewkey", this.par.viewkey);
		if(this.p_key != -1) data.append("p_key", this.p_key);
		var changes = this.p_key == -1;
		for(var cell of this.cells){
			if(cell.add_change(data, this.p_key == -1)){
				changes = true;
			}
		}
		if(changes == false) return;
		var req_obj = get_ajax_object(this.par.srvr);
		if(this.p_key == -1){
			req_obj.onload = this.get_onload_add(this);
		}
		else{
			req_obj.onload = this.get_onload_mod(this);
		}
		req_obj.send(data);
	}

	/*
	 * On load of delete of given row deletes the row from Table_obj and
	 * from HTML <table> on success.
	 */
	get_onload_rm(row){
		return function (evt){
			console.log(this.status);
			console.log(this.response);
			if(this.status === 200){
				for(let idx in row.par.table.children){
					if(row.row == row.par.table.children[idx]){
						row.par.table.deleteRow(idx);
					}
				}
				--row.par.rlen;
				delete row.par.rows[row.rid];
				row.par.color_table();
			}
			else{
				alert("Error processing request [rm]\nHTML status code: " + this.status + "\nResponse:\n" + this.response + "\n");
			}
		}
	}

	/*
	 * Sends request to server to remove row if it is selected.
	 */
	rm(){
		if(this.selected == false) return;
		if(this.p_key == -1){	//	row doesn't exist in db yet
			for(let idx in this.par.table.children){
				if(this.row == this.par.table.children[idx]){
					this.par.table.deleteRow(idx);
				}
			}
			--this.par.rlen;
			delete this.par.rows[this.rid];
			this.par.color_table();
			return;
		}
		var data = new FormData();
		data.append("action", "rm");
		data.append("t_name", this.par.tid);
		data.append("key", this.par.key);
		if(this.par.viewkey != undefined) data.append("viewkey", this.par.viewkey);
		data.append("p_key", this.p_key);
		var req_obj = get_ajax_object(this.par.srvr);
		req_obj.onload = this.get_onload_rm(this);
		req_obj.send(data);
	}

	/*
	 * Reverses edits to all cells.
	 */
	reverse_edits(){
		for(var cell of this.cells)
			cell.reverse_edits();
	}

	/*
	 * Adds checkbox for selecting the row. Value of this.selected is updated
	 * on change of said checkbox and denotes whether the row is selected, as
	 * well as sets class selected for visual aid.
	 */
	show_select(){
		this.checkbox = document.createElement("input");
		this.checkbox.type = "checkbox";
		this.checkbox.checked = this.selected;
		this.checkbox.onchange = function(){
			this.selected = this.checkbox.checked;
			if(this.selected){
				this.row.classList.add("selected");
			}
			else{
				this.row.classList.remove("selected");
			}
		}.bind(this);
		let cell = document.createElement("td");
		cell.appendChild(this.checkbox);
		this.row.appendChild(cell);
	}

	/*
	 * Removes checkbox for selecting the row.
	 */
	hide_select(){
		this.row.deleteCell(this.clen);
	}

	/*
	 * Colors the row according to num via CSS.
	 */
	color_row(num){
		this.row.classList.remove("trow_" + ((+num%2!=0)?"even":"odd"));
		this.row.classList.add("trow_" + ((+num%2==0)?"even":"odd"));
	}

}
