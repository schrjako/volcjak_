class Table{
	tid;	//	table id (DOM) and name
	edit_obj;	//	Edit object
	row_title;	//	titlerow
	rows;	//	array of rows
	rlen;	//	number of rows
	nrid;	//	next rid
	clen;	//	number of cells
	key;	//	authentication key
	viewkey;	//	viewkey for kontrola

	add;	//	user has permission to add rows
	del;	//	user has permission to delete rows
	hash;	//	user has permission to get hash-values to the given rows

	srvr;	//	relative server url to send AJAX requests to

	table;	//	reference to DOM table object
	but_toggle;	//	button for toggling select
	but_add;	//	button for adding rows
	but_save;	//	button for saving changes
	but_rm;	//	button for removing selected rows
	but_rev;	//	button for reversing edits
	but_hash;	//	button for getting hashes
	select_on;	//	bool denoting whether selection mode is on
	links_on;	//	bool denoting whether links are shown

	constructor({tid, rlen, clen, key, viewkey = undefined, add = false, del = false, hash = false, srvr, edit_obj, rows}={}){
		this.tid = tid;
		this.rlen = rlen;
		this.nrid = rlen;
		this.clen = clen;
		this.key = key;
		this.viewkey = viewkey;

		this.add = add;
		this.del = del;
		this.hash = hash;

		this.srvr = srvr;

		this.edit_obj = edit_obj;
		this.rows = rows;
		this.select_on = false;
		this.set_as_parent();
		this.show();
	}

	/*
	 * Adds parents to Edit, Editcol, Row, and Cell.
	 */
	set_as_parent(){
		this.edit_obj.set_parent(this);
		for(var rid in this.rows){
			this.rows[rid].set_parent(this);
		}
	}

	/*
	 * Appends row with index rid to table.
	 */
	append_row(rid, ghost_row = false){
		this.table.appendChild(this.rows[rid].create_row(
			{rid: rid, id_prefix: "datarow", ghost_row: ghost_row}
		));
	}

	/*
	 * Adds rows and cells to table, as well as the button for saving.
	 */
	show(){
		this.table = document.createElement("table");
		this.table.id = this.tid;
		$("body").appendChild(this.table);

		this.row_title = this.edit_obj.create_title_row();
		this.table.appendChild(this.row_title);
		for(var rid in this.rows){
			this.append_row(rid);
		}
		this.color_table();

		//	Button for toggling selection mode.
		this.but_toggle = create_button({value: "pokaži označevanje",
			onclick: this.toggle_select.bind(this)});
		
		//	Button for adding a row.
		if(this.add)
			this.but_add = create_button({value: "dodaj vrstico", onclick: this.add_row.bind(this)});
		
		//	Button for saving changes.
		var save_button = create_button({value: "shrani", onclick: this.save.bind(this)});

		//	Button for deleting selected rows.
		if(this.del)
			var rm_button = create_button({value: "briši označene", ondblclick: this.rm.bind(this)});

		//	Button for reversing current changes.
		this.but_rev = create_button({value: "prekliči urejanje",
			ondblclick: this.reverse_edits.bind(this)});

		//	Button for getting links to specific rows.
		if(this.hash)
			this.but_hash = create_button({value: "pokaži povezave",
				onclick: this.toggle_links.bind(this)});
	}

	/*
	 * Adds a new row to the end of the table.
	 */
	add_row(){
		var cells = [];
		for(var cid = 0; cid < this.clen; ++cid){
			cells[cid] = new Cell(this.nrid, cid);
		}
		this.rows[this.nrid] = new Row(this.nrid, -1, this.clen, cells);
		this.rows[this.nrid].set_parent(this);
		this.append_row(this.nrid, true);
		if(this.select_on){
			this.rows[this.nrid].show_select();
		}
		++this.nrid;
		++this.rlen;
		this.color_table();
	}

	/*
	 * Saves the changes row for row.
	 */
	save(){
		console.log("saving data");
		for(var rid in this.rows){
			this.rows[rid].save();
		}
	}

	/*
	 * Removes selected lines from table.
	 */
	rm(){
		console.log("removing rows");
		for(var rid in this.rows){
			this.rows[rid].rm();
		}
	}

	/*
	 * Reverses edits.
	 */
	reverse_edits(){
		console.log("reversing edits");
		for(var rid in this.rows){
			if(this.rows[rid].p_key == -1){
				this.rows[rid].selected = true;
				this.rows[rid].rm();
			}
			else{
				this.rows[rid].reverse_edits();
			}
		}
	}

	/*
	 * Toggles the selection menu.
	 */
	toggle_select(){
		console.log("toggling select");
		if(this.select_on == false){
			this.but_toggle.value = "Skrij označevanje";
			this.select_on = true;
			let cell = document.createElement("th");
			cell.innerHTML = "Označi";
			this.row_title.appendChild(cell);
			for(var rid in this.rows){
				this.rows[rid].show_select();
			}
		}
		else{
			this.but_toggle.value = "Pokaži označevanje";
			this.select_on = false;
			this.row_title.deleteCell(this.clen);
			for(var rid in this.rows){
				this.rows[rid].hide_select();
			}
		}
	}

	/*
	 * Toggles link display.
	 */
	toggle_links(){
		console.log("toggling links");
		if(this.links){
			console.log("hiding links");
			this.but_hash.value = "pokaži povezave";
			this.links = false;
		}
		else{
			console.log("showing links");
			this.but_hash.value = "skrij povezave";
			this.links = true;
		}
		for(var rid in this.rows){
			this.rows[rid].set_inner_cell_HTML();
		}
	}

	/*
	 * Colors the rows in the table.
	 */
	color_table(){
		let num = 0;
		for(var rid in this.rows){
			this.rows[rid].color_row(++num);
		}
	}

}
