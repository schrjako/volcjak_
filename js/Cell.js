class Cell{
	cid;	//	column id
	rid;	//	row id
	id;	//	DOM id
	value_old;	//	value in db
	value_new;	//	value changed by user, equal to value_old when not changed
	pars;	//	parents ([0] - Row, [1] - Table)
	edit;	//	reference to Editcol object associated with cell
	cell;	//	reference to DOM cell object
	last_timestamp;	//	timestamp of the last mousedown/touchstart on this cell without
	               	//	its pair mouseup/touchend

	constructor(rid, cid, value_old = ""){
		this.rid = rid;
		this.cid = cid;
		this.id = mkid(this.rid, this.cid);
		this.value_old = value_old;
		this.value_new = value_old;
		this.last_timestamp = undefined;
	}

	/*
	 * Sets the cell's parents (Row and Table).
	 */
	set_parents(pars){
		this.pars = pars;
		this.edit = this.pars[1].edit_obj.get_edit(this.cid);
		if(this.value_new === ""){
			this.value_new = this.edit.get_default_value();
		}
	}

	/*
	 * Creates and returns <input> or <select> element for the cell.
	 */
	get_input_el(is_fk){
		switch(this.edit.type){
			case "select":
				this.input_el = document.createElement("select");
				for(let oid in this.edit.options){
					let option = document.createElement("option");
					option.value = oid;
					option.innerHTML = this.edit.options[oid];
					this.input_el.appendChild(option);
				}
				this.input_el.value = this.value_new;
				break;
			default:
				this.input_el = document.createElement("input");
				this.input_el.type = this.edit.type;
				if(this.edit.type == "checkbox"){
					this.input_el.checked = this.value_new;
				}
				else{
					this.input_el.value = this.value_new;
				}
				if(this.edit.type == "time") this.input_el.step = 1;
				if(this.edit.len != undefined) this.input_el.maxlength = this.edit.len;
				if(this.edit.low != undefined) this.input_el.min = this.edit.low;
				if(this.edit.high != undefined) this.input_el.max = this.edit.high;
		}
		if(!is_fk) this.input_el.onblur = this.edit_end;
		return this.input_el;
	}

	/*
	 * Sets the cell value to default (used mainly for date and time columns).
	 */
	set_default(){
		this.value_new = this.edit.get_default_value();
		this.set_innerHTML();
	}

	/*
	 * Returns the function to be run on mousedown or touchstart.
	 * If the touch/click hasn't been canceled until timediff ms elapse,
	 * the value of the cell is set to default.
	 */
	get_onmousedown(cell, timediff){
		return function(){
			console.log("mouse down", Date.now());
			cell.last_timestamp = Date.now();
			setTimeout(function(){
				if(cell.last_timestamp != undefined && Date.now() - cell.last_timestamp >= timediff){
					console.log("setting default", cell, Date.now(), Date.now()-cell.last_timestamp);
					cell.set_default();
				}
				cell.last_timestamp = undefined;
			}, timediff);
		};
	}

	/*
	 * Cancels mouse/touch hold and prevents the cell's value being set to default.
	 */
	get_onmouseup(cell){
		return function(){
			console.log("mouse up", Date.now());
			cell.last_timestamp = undefined;
		};
	}

	/*
	 *
	 */
	assign_mouse_touch_action(action = "enable"){
		if(this.edit.type != "date" && this.edit.type != "time") return;
		if(this.edit.default_value == undefined) return;
		if(action == "disable"){
			//	desktop
			this.cell.onmousedown = undefined;
			this.cell.onmouseup = undefined;
			//	mobile
			this.cell.ontouchstart = undefined;
			this.cell.ontouchmove = undefined;
			this.cell.ontouchend = undefined;
			this.cell.ontouchleave = undefined;
			this.cell.ontouchcancel = undefined;
		}
		if(action == "enable"){
			//	desktop
			this.cell.onmousedown = this.get_onmousedown(this, desktop_hold_time);
			this.cell.onmouseup = this.get_onmouseup(this);
			//	mobile
			this.cell.ontouchstart = this.get_onmousedown(this, mobile_hold_time);
			this.cell.ontouchmove = this.get_onmouseup(this);
			this.cell.ontouchend = this.get_onmouseup(this);
			this.cell.ontouchleave = this.get_onmouseup(this);
			this.cell.ontouchcancel = this.get_onmouseup(this);
		}
	}

	/*
	 * Creates HTML cell.
	 */
	create_cell(ghost_row = false){
		this.cell = document.createElement("td");
		this.cell.id = this.id;
		this.set_innerHTML();
		if(ghost_row){
			this.cell.innerHTML = "";
			this.cell.appendChild(this.get_input_el(true));
		}
		else{
			this.assign_mouse_touch_action();
			if(this.edit.editable){
				this.cell.ondblclick = this.get_edit_start();
			}
		}
		return this.cell;
	}

	/*
	 * Sets the cell's inner HTML to its value.
	 */
	set_innerHTML(){
		switch(this.edit.type){
			case "select":
				this.cell.innerHTML = this.edit.options[this.value_new];
				break;
			case "checkbox":
				this.cell.innerHTML = this.value_new?"da":"ne";
				break;
			default:
				this.cell.innerHTML = this.value_new;
		}
		if(this.edit.link && this.pars[1].links){
			this.cell.innerHTML = "<a href='table.php?key=" + this.pars[1].tid + ":" +
				this.pars[0].hash + "&name=" + this.pars[1].tid + "'>" + this.cell.innerHTML + "</a>";
		}
	}

	/*
	 * Starts editing for the cell by replacing the cell's innerHTML for
	 * an <input> or <select> element.
	 */
	get_edit_start(){
		return (event)=>{
			console.log('edit start', this);
			this.cell.innerHTML = "";
			this.cell.appendChild(this.get_input_el());
			this.assign_mouse_touch_action("disable");
			this.input_el.focus();
		};
	}

	/*
	 * Ends editing of the cell.
	 */
	get edit_end(){
		return (event)=>{
			console.log('edit end', this);
			if(this.edit.type == "checkbox"){
				this.value_new = this.input_el.checked;
			}
			else{
				this.value_new = this.input_el.value;
			}
			this.set_innerHTML();
			this.assign_mouse_touch_action();
		};
	}

	/*
	 * Add changes to formData object if it was changed, returns true on changed data and
	 * false on unchanged.
	 */
	add_change(data, ghost_row){
		let added_new_value = false;
		if(ghost_row){
			if(this.edit.type == "checkbox"){
				this.value_new = this.input_el.checked;
			}
			else{
				this.value_new = this.input_el.value;
			}
			data.append(this.edit.name,
				(this.edit.type == "checkbox") ? (this.value_new ? "1" : "0") : (this.value_new));
		}
		else if(this.value_old != this.value_new){
			data.append(this.edit.name,
				(this.edit.type == "checkbox") ? (this.value_new ? "1" : "0") : (this.value_new));
			added_new_value = true;
		}
		else if(this.edit.allsend){
			if(this.edit.type == "checkbox"){	//	converting bool to int
				data.append(this.edit.name, this.value_new ? "1" : "0");
			}
			else{
				data.append(this.edit.name, this.value_new);
			}
		}
		return added_new_value;
	}

	/*
	 * Applies changes made by user, called when they are successfully processed by the server.
	 */
	apply_change(){
		this.value_old = this.value_new;
	}

	/*
	 * Reverses edits.
	 */
	reverse_edits(){
		this.value_new = this.value_old;
		this.set_innerHTML();
	}

}
