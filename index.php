<?php

require_once("./db/config.php");
require_once("./db/utils.php");
require_once("./db/Key.php");
require_once("./db/tables.php");

$name = "Index";

if(!isset($_GET['key'])){
	header("HTTP/1.0 400 Bad Request");
	exit("Missing a key!\n");
}

$key = new Key($_GET['key']);

if(!$key->valid){
	header("HTTP/1.0 400 Bad Request");
	exit("Error creating key!\n");
}

?>
<!DOCTYPE html>
<html>
<?php
	include_once("head.php");
?>
	<body id="body">
		<h1> Volčjak: </h1>
<?php
$hash = $key->get_key();
$viewhash = id_to_hash($key->t_name . "_view", $key->id);
switch($key->t_name){
	case "tekmovanje":
		print("<p><a href='./table.php?key=$hash&name=tekmovanje'>struktura tekmovanja</a></p>\n");
		print("<p><a href='./kontrola.php?key=$hash&viewkey=tekmovanje:$viewhash'>kontrole</a></p>\n");
		print("<p><a href='./calc.php?key=$hash'>računanje</a></p>\n");
		print("<p><a href='./result.php?viewkey=tekmovanje:$viewhash&sum'>rezultati</a></p>\n");
		break;
}
?>
	</body>
</html>
