<?php

require_once("./db/constants.php");
require_once("./db/Table.php");

$tables["KT"] = new Table(
	"KT",
	[
		"id" =>
			new Member("id", "INT", flags: "NOT NULL", auto_increment: true),
		"id_tekm" =>
			new Member("id_tekm", "INT", display_name: "tekmovanje", ft_name: "tekmovanje"),
		"ime" =>
			new Member("ime", "VARCHAR($slen[0])"),
		"lokacija" =>
			new Member("lokacija", "VARCHAR($slen[1])"),
		"kontrolorji" =>
			new Member("kontrolorji", "VARCHAR($slen[2])"),
		"kontakt" =>
			new Member("kontakt", "VARCHAR($slen[1])")
	],
	new Perms("tekmovanje", "id_tekm",
		[
			"tekmovanje" => new Perm("tekmovanje", ["ime", "lokacija", "kontrolorji", "kontakt"], add: true, del: true, hash: true),
			"KT" => new Perm("KT", ["ime", "lokacija", "kontrolorji", "kontakt"])
		]
	)
);

?>
