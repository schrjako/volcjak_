<?php

require_once("./db/Member.php");
require_once("./db/Perms.php");
require_once("./db/Key.php");
require_once("./db/utils.php");

/*
 * Holds the structure of a table - both its MySQL properties and other relevant data.
 */
class Table{

	/*
	 * $name is the table's name in database.
	 * $members are its columns.
	 * $p_key is/are the column/s that act as primary key, defaults to 'id'.
	 * $perms denote permissions associated with keys for tables.
	 */
	public function __construct($name, $members, $perms, $p_key = "id", $children = []){
		$this->name = $name;
		$this->members = $members;
		$this->p_key = $p_key;
		$this->perms = $perms;
		$this->children = $children;
		$this->add_parents($this->perms->parent);
	}

	/*
	 * Adds all the parents up the DAG who have the same permissions as $this->perms->parent.
	 */
	private function add_parents($ft_name){
		global $tables;
		if($ft_name == "tekmovanje" || $ft_name == "php" || !array_key_exists($ft_name, $tables))
			return;
		$this->perms->add_parent($tables[$ft_name]->perms->parent);
		$this->add_parents($tables[$ft_name]->perms->parent);
	}

	/*
	 * Adds key to Table.
	 */
	public function set_key($key, $view = false){
		$this->key = new Key($key, $view);
	}

	/*
	 * Returns true or false on given permission with accordance to the current $this->key.
	 */
	public function permits($permission){
		return $this->perms->permits($this->key, $permission);
	}

	/*
	 * Returns a MySQL statement which creates the given table as defined.
	 * Can be indented by defining larger $indent.
	 */
	public function get_table_create($indent = "\t"){
		$table_create = "CREATE TABLE IF NOT EXISTS $this->name (\n{$indent}";
		foreach($this->members as $member){
			$create_definition = $member->get_create_definition();
			$table_create .= "$create_definition,\n{$indent}";
		}
		$table_create .= "PRIMARY KEY ($this->p_key)\n);\n";
		return $table_create;
	}

	/*
	 * Returns p_key with prefixed table name.
	 */
	private function get_extended_p_key($t_name = ""){
		if($t_name == "") $t_name = $this->name;
		sscanf($this->p_key, "%[^,], %s", $first, $second);
		$extended = "$t_name.$first";
		if($second != "") $extended = "CONCAT($extended, ', ', $t_name.$second)";
		return $extended;
	}

	/*
	 * Returns PRIMARY KEY value for given row.
	 */
	private function get_p_key_val($rid){
		$second = "";
		sscanf($this->p_key, "%[^,], %s", $first, $second);
		$p_key = $this->members[$first]->values[$rid];
		if($second != ""){
			$p_key .= ", " . $this->members[$second]->values[$rid];
		}
		return $p_key;
	}

	/*
	 * Gets the rows the user has permission to edit, given the initialized Key.
	 * If $ids is not empty the rows are limited to those with primary keys contained
	 * in $ids.
	 *
	 * Returns true on success and false on failure.
	 */
	private function get_data($ids = []){
		if($this->got_data) return true;
		global $link;
		$params = ["stmt", ""];
		$res_args = ["stmt"];
		$this->clen = 0;
		$what = [];
		foreach($this->members as $m_name => $member){
			$res_args[] = &$$m_name;
			$s_what = "$this->name.$member->name";
			$what[$s_what] = $s_what;
			if($member->ft_name != null){
				$member->get_display_values($this->key);
				$ft_name = $member->prefix . $member->ft_name;
				if($member->prefix != "") $s_join = "$member->ft_name AS $ft_name";
				else $s_join = $member->ft_name;
				$join[$s_join] = $s_join;
				$s_on = "$this->name.$m_name = $ft_name.id";
				$on[$s_on] = $s_on;
				$member->root_cross_join($join, $on);
			}
			++$this->clen;
			if($member->type == "DATETIME") ++$this->clen;
			if($this->p_key == $member->name) --$this->clen;
		}
		get_join_on($this->name, $join, $on);
		$idswhere = [];
		foreach($ids as $id){
			$s_where = "(" . $this->get_extended_p_key() . ") = (?)";
			$idswhere[$s_where] = $s_where;
			$params[1] .= "s";
			$params[] = $id;
		}
		if(count($idswhere)){
			$s_where = "(" . implode(" OR ", $idswhere) . ")";
			$where[$s_where] = $s_where;
		}
		$this->key->root_cross_join($join, $on);
		$this->key->get_where($where);
		unset($join[$this->name]);
		$q = "SELECT " . implode(", ", $what) . "\n" .
			"\tFROM $this->name";
		if($join != null) $q .= "\n\tJOIN (" . implode(", ", $join) . ")";
		if($on != null) $q .= "\n\tON (" . implode(" AND ", $on) . ")";
		if($where != null) $q .= "\n\tWHERE (" . implode(" AND ", $where) . ")";
		$q .= ";\n";
		if(!($params[0] = mysqli_prepare($link, $q))){
			print("Error preparing q: [\n$q]!\n");
			print_r($params);
			return false;
		}
		$bind_args = array_merge([$sel_stmt, str_repeat("s", count($ids))], $ids);
		if(count($ids))
			if(!call_user_func_array("mysqli_stmt_bind_param", $params)){
				print("Error binding parameters!\n");
				return false;
			}
		$res_args[0] = &$params[0];
		if(!mysqli_stmt_execute($params[0])){
			print("Error executing statement!\n");
			return false;
		}
		if(!call_user_func_array("mysqli_stmt_bind_result", $res_args)){
			print("Error binding result!\n");
			return false;
		}
		if(!mysqli_stmt_store_result($params[0])){
			print("Error storing result!\n");
			return false;
		}
		$this->rlen = 0;
		while(mysqli_stmt_fetch($params[0])){
			foreach($this->members as $m_name => $member){
				$member->values[$this->rlen] = $$m_name;
			}
			++$this->rlen;
		}
		mysqli_stmt_close($params[0]);
		$this->got_data = true;
		return true;
	}

	/*
	 * Adds array's data to table, used for checking the correctness of FOREIGN KEYs.
	 *
	 * Returns true on success and false on failure.
	 */
	public function add_data($array){
		global $link;
		if(array_key_exists("p_key", $array))
			if(!$this->get_data([$array["p_key"]]))	//	p_key not ok
				return false;
		$params = ["ftmt"];
		sscanf($this->p_key, "%[^,], %s", $f, $s);
		$this->key->root_cross_join($join, $on);
		$this->key->get_where($where);
		foreach($this->members as $m_name => $member){
			if(!$member->auto_increment){
				if($member->type == "DATETIME" && array_key_exists("d_" . $m_name, $array) &&
						array_key_exists("t_" . $m_name, $array)){
					$this->members[$m_name]->values[0] = "{$array["d_" . $m_name]} {$array["t_" . $m_name]}";
				}
				else if(array_key_exists($m_name, $array)){
					$this->members[$m_name]->values[0] = $array[$m_name];
				}
			}
			if($member->ft_name != null){
				$member->root_cross_join($join, $on);
				$member->get_param_where($params, $where);
			}
		}
		unset($join[$this->name]);
		$q = "SELECT tekmovanje.id as tid FROM $this->name";
		if($join != null && count($join)) $q .= "\n\tJOIN (" . implode(", ", $join) . ")";
		if($on != null && count($on)) $q .= "\n\tON (" . implode(" AND ", $on) . ")";
		if($where != null && count($where)) $q .= "\n\tWHERE  (" . implode(" AND ", $where) . ")";
		$q .= ";\n";
		if(!$params[0] = mysqli_prepare($link, $q)){
			print("Error preparing query [\n$q]!\n");
			return false;
		}
		if(count($params)>2){
			if(!call_user_func_array("mysqli_stmt_bind_param", $params)){
				print("Error binding parameters!\n");
				return false;
			}
		}
		if(!mysqli_stmt_execute($params[0])){
			print("Error executing prepared statement!\n");
			return false;
		}
		if(!mysqli_stmt_bind_result($params[0], $tid)){
			print("Error binding result!\n");
		}
		if(!mysqli_stmt_fetch($params[0])){
			mysqli_stmt_close($params[0]);
			print("FOREIGN KEYs don't match up with key!\n");
			return false;
		}
		mysqli_stmt_close($params[0]);
		return true;
	}

	/*
	 * Updates rows from $l (low) to $h (high) in db according to data in members' values arrays.
	 *
	 * Returns true on success and false on failure.
	 */
	public function update_data($l = 0, $h = 1){
		global $link;
		$q = "";
		sscanf($this->p_key, "%[^,], %s", $f, $s);
		$params = ["stmt", ""];
		$set = [];
		foreach($this->members as $m_name => $member){
			if(!$this->permits($m_name)) continue;
			$set[] = "$this->name.$m_name = ?";
			$params[1] .= $member->get_MySQL_type_char();
			$params[] = &$member->values[$l];
		}
		$q = "UPDATE $this->name SET " . implode(", ", $set) . " WHERE (" .
			$this->get_extended_p_key() . ") = (?);";
		$params[1] .= "s";
		$params[] = $this->get_p_key_val($l);
		if(!$params[0] = mysqli_prepare($link, $q)){
			print("Error preparing query [$q]!\n");
			return false;
		}
		if(!call_user_func_array("mysqli_stmt_bind_param", $params)){
			print("Error binding parameters!\n");
			return false;
		}
		for($rid = $l; $rid < $h; ++$rid){
			$cid = 2;
			foreach($this->members as $m_name => $member){
				if(!$this->permits($m_name)) continue;
				$params[$cid++] = &$member->values[$rid];
			}
			$params[$cid] = $this->get_p_key_val($rid);
			if(!mysqli_stmt_execute($params[0])){
				print("Error executing statement!\n");
				print("MySQL error: {$params[0]->error}\n");
				return false;
			}
		}
		return true;
	}

	/*
	 * Deletes row with PRIMARY KEY p_key.
	 */
	public function row_delete($p_key){
		global $link;
		get_join_on($this->name, $join, $on);
		$q = "DELETE $this->name.* FROM $this->name\n" .
			"\tJOIN (" . implode(", ", $join) . ")\n" .
			"\tON (" . implode(" AND ", $on) . ")\n" .
			"\tWHERE {$this->key->t_name}.id = {$this->key->id} AND (" . $this->get_extended_p_key() .
			") = (?);\n";
		if(!($stmt = mysqli_prepare($link, $q))){
			print("Error preparing q: [\n$q]!\n");
			return false;
		}
		if(!mysqli_stmt_bind_param($stmt, "s", $p_key)){
			print("Error binding params to q: [\n$q], ? = $p_key!\n");
			return false;
		}
		if(!mysqli_stmt_execute($stmt)){
			print("Error executing statement!\n");
			print("MySQL error: $stmt->error\n");
			return false;
		}
		if($stmt->affected_rows != 1){
			print("Query [\n$q] affected {$stmt->affected_rows} rows instead of 1!\n");
			return false;
		}
		return true;
	}

	/*
	 * Adds row to db. On success returns the new id, on failure -1.
	 */
	public function row_add(){
		global $link;
		$params = ["stmt", ""];
		foreach($this->members as $m_name => $member){
			if($this->p_key == $m_name) continue; 
			$cols[] = $m_name;
			$vals[] = "?";
			$params[1] .= $member->get_MySQL_type_char();;
			$params[] = $member->values[0];
		}
		if(count($cols) == 0 || count($vals) == 0) return -1;
		
		$q = "INSERT INTO $this->name\n" .
			"\t(" . implode(", ", $cols) . ")\n" .
			"\tVALUES\n" .
			"\t(" . implode(", ", $vals) . ");\n";

		if(!($params[0] = mysqli_prepare($link, $q))){
			print("Error preparing q: [\n$q]!\n");
			print_r($params);
			return -1;
		}

		if(!call_user_func_array("mysqli_stmt_bind_param", $params)){
			print("Error binding parameters!\n");
			return -1;
		}

		if(!mysqli_stmt_execute($params[0])){
			print("Error executing statement!\n");
			print("MySQL error: {$params[0]->error}\n");
			return -1;
		}

		return mysqli_insert_id($link);
	}

	/*
	 * Returns hash for specified id.
	 */
	public function id_to_hash($id){
		id_to_hash($this->name, $id);
	}

	/*
	 * Returns javascript object describing columns - what they contain and whether they can
	 * be edited. For further explanation look in 'js/Edit.js'.
	 */
	private function get_Edit_obj($indent = ""){
		$this->get_data();
		$Edit_obj = [];
		foreach($this->members as $m_name => $member){
			if($this->p_key == $m_name) continue;
			$s_editcol = "new Editcol(" . $member->get_edit_desc($this->permits($m_name),
				is_p_fk: ($m_name == $this->perms->p_c_name), indent: $indent . "\t") . ")";
			$Edit_obj[$s_editcol] = $s_editcol;
		}
		return "{$indent}edit_obj: new Edit([\n{$indent}\t" . implode(",\n{$indent}\t", $Edit_obj) .
			"\n{$indent}])";
	}

	/*
	 * Returns js Row definition.
	 * For further explanation look in 'js/Row.js'.
	 */
	private function get_Table_Row($rid){
		$Table_Cells = [];
		$cid = 0;
		foreach($this->members as $member){
			if($this->p_key == $member->name) continue;
			$Table_Cells[] = $member->get_Table_Cell($rid, $cid);
			++$cid;
		}
		$p_key = $this->get_p_key_val($rid);
		$row = "new Row($rid, '" . $p_key . "', $this->clen, [" .  implode(", ", $Table_Cells) . "]";
		if($this->permits("hash")) $row .= ", '" . $this->id_to_hash($p_key) . "'";
		return $row . ")";
	}

	/*
	 * Returns js Rows definitions - object of Row objects.
	 */
	private function get_Table_Rows($indent = ""){
		$this->get_data();
		$Table_Rows = [];
		for($r = 0; $r < $this->rlen; ++$r){
			$Table_Rows[] = "$r: " . $this->get_Table_Row($r);
		}
		if(count($Table_Rows)!=0) return "${indent}rows: {\n{$indent}\t" .
			implode(",\n{$indent}\t", $Table_Rows) . "\n{$indent}}";
		return "{$indent}rows: {}";
	}

	/*
	 * Returns js Table definition.
	 * For further explanation look in 'js/Table.js'.
	 */
	public function get_Table_def($tid = "", $indent = ""){
		if($tid == "") $tid = $this->name;
		if(!$this->get_data()){
			exit("Error getting data!<br>\n");
		}
		$Table_def = "new Table({tid: '$tid', rlen: $this->rlen, clen: $this->clen, " .
			"key: '" . $this->key->get_key() . "'";
		if($this->permits("add")) $Table_def .= ", add: true";
		if($this->permits("del")) $Table_def .= ", del: true";
		if($this->permits("hash")) $Table_def .= ", hash: true";
		$Table_def .= ", srvr: 'db/row.php',\n";
		$Table_def .= $this->get_Edit_obj(indent: $indent . "\t") . ",\n";
		$Table_def .= $this->get_Table_Rows(indent: $indent . "\t") . "\n{$indent}});\n";
		return $Table_def;
	}

	/*
	 * Returns list of HTML links to other tables to which the given key has permissions.
	 */
	public function get_links($indent = ""){
		global $tables;
		$links = "";
		foreach($tables as $t_name => $table){
			if(array_key_exists($this->key->t_name, $table->perms->perms)){
				$links .= "$indent<p><a href='table.php?key=" . $this->key->get_key() .
					"&name=$t_name'>$t_name</a></p>\n";
			}
		}
		return $links;
	}

	/*
	 * Returns buttons that call db/calc.php when pressed which calculates
	 * results for the indicated scope.
	 */
	public function get_calc_buts($indent = "", $first = true){
		if($this->name != "proga" && $this->name != "kategorija" && $this->name != "tekmovanje") return "";
		if($this->key->t_name != "proga" && $this->key->t_name != "kategorija" && $this->key->t_name != "tekmovanje") return "";
		if(!$this->get_data()){
			header("HTTP/1.0 500 Internal Server Error");
			exit("Error getting data!<br>\n");
		}
		$buts = [];
		foreach($this->members["id"]->values as $idx => $id){
			$buts[] = "create_button({value: '{$this->members['ime']->values[$idx]}', " .
				"onclick: calc.bind({key: '" . $this->key->get_key() . "', table: '$this->name', id: {$this->members['id']->values[$idx]}})});";
		}
		global $tables;
		if($this->name == "proga" && $this->key->t_name != "proga"){
			$tables['kategorija']->key = &$this->key;
			$buts[] = $tables['kategorija']->get_calc_buts(indent: $indent, first: false);
		}
		if($this->name == "kategorija" && $this->key->t_name == "tekmovanje"){
			$tables['tekmovanje']->key = &$this->key;
			$buts[] = $tables['tekmovanje']->get_calc_buts(indent: $indent, first: false);
		}
		if($first){
			return "$indent<script type='text/javascript'>\n" .
				$indent . "\t" . implode("\n$indent\t", $buts) . "\n" .
				"$indent</script>\n";
		}
		return implode("\n$indent\t", $buts);
	}

}

?>
