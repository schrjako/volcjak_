<?php

/*
 * String length table for ease of modifying.
 */
$slen = [
	0 => 20,
	1 => 100,
	2 => 300
];

/*
 * Names of all tables in creating and reverse dropping order.
 */
$table_names = ["tekmovanje", "KT", "kategorija", "proga", "ekipa", "start", "naloga", "dogodek", "rezultat"];

/*
 * Length of the hash used.
 */
$hash_len = 40;

/*
 * Points earned with help are multiplied with this constant.
 */
$pomoc_faktor = 0.6;

?>
