<?php

require_once("./db/constants.php");
require_once("./db/Table.php");

$tables["dogodek"] = new Table(
	"dogodek",
	[
		"id_ekipa" =>
			new Member("id_ekipa", "INT", display_name: "ekipa", ft_name: "ekipa"),
		"id_naloga" =>
			new Member("id_naloga", "INT", display_name: "naloga", ft_name: "naloga"),
		"vrednost" =>
			new Member("vrednost", "INT", def: "0"),
		"cas" =>
			new Member("cas", "DATETIME", display_name: "čas"),
		"pomoc" =>
			new Member("pomoc", "BOOL", display_name: "pomoč", def: "false"),
	],
	new Perms("naloga", "id_naloga",
		[
			"naloga" => new Perm("naloga", ["vrednost", "cas", "pomoc"], add: true, del: true, hash: true),
			"ekipa" => new Perm("ekipa", [], view: true)
		]
	),
	p_key: "id_ekipa, id_naloga"
);

?>
