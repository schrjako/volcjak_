<?php

/*
 * Includes all table definitions and puts them in the array $tables.
 */

require_once("./db/constants.php");

foreach($table_names as $t_name){
	require_once("./db/$t_name.php");
}

?>
