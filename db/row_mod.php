<?php

chdir("..");

require_once("./db/connect.php");
require_once("./db/tables.php");

print_r($_POST);

$t_name = $_POST['t_name'];

if(!array_key_exists($t_name, $tables)){
	header("HTTP/1.0 400 Bad Request");
	exit("Table name is incorrect, table '$t_name' doesn't exist!\n");
}

$table = $tables[$t_name];
$table->set_key($_POST['key']);
if(!isset($table->key) || $table->key->valid == false){
	header("HTTP/1.0 400 Bad Request");
	exit("The key {$_POST['key']} doesn't seem to be valid, check the correctness of your link!\n");
}

if(!array_key_exists($table->key->t_name, $table->perms->perms)){
	header("HTTP/1.0 400 Bad Request");
	exit("Key seems to be valid, but doesn't have permissions in table $table->name!\n");
}

if(!$table->add_data($_POST)){
	header("HTTP/1.0 400 Bad Request");
	exit("Error while adding data!\n");
}

if(!$table->update_data()){
	header("HTTP/1.0 400 Bad Request");
	exit("Error while updating data!\n");
}

print("Success!");

?>
