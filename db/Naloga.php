<?php

/*
 * Represents one naloga in the Kontrola class.
 */
class Naloga{

	/*
	 * Relevant data to naloga.
	 */
	public function __construct($row, &$clen, &$parent){
		$this->id = $row->id;
		$this->ime = $row->ime;
		$this->type = $row->type;
		$this->max_tocke = $row->max_tocke;
		$this->faktor = $row->faktor;
		$this->moznost_pomoci = $row->moznost_pomoci;
		$this->par = $parent;
		if($this->moznost_pomoci){
			++$clen;
			$this->pomoc = [];
		}
		$this->dttm = [];
		$this->date = [];
		$this->time = [];
		switch($this->type){
			case "KT":
				$parent->in = $this->id;
				break;
			case "naloga":
				$this->vrednost = [];
				$parent->mid = $this->id;
				break;
			case "hitrostna_start":
				$parent->out = $this->id;
				break;
			case "hitrostna_cilj":
				$parent->in = $this->id;
				break;
			case "cilj":
				$parent->in = $this->id;
				$parent->par->cilj = &$this;
				break;
		}
	}

	/*
	 * Adds data from given object $row.
	 */
	public function add_data($row){
		$this->dttm[$row->id_ekipa] = $row->cas;
		$this->date[$row->id_ekipa] = substr($row->cas, 0, 10);
		$this->time[$row->id_ekipa] = substr($row->cas, 11, 8);
		switch($this->type){
			case "KT": break;
			case "naloga":
				$this->vrednost[$row->id_ekipa] = $row->vrednost;
				break;
			case "hitrostna_start": break;
			case "hitrostna_cilj": break;
		}
		if($this->moznost_pomoci){
			$this->pomoc[$row->id_ekipa] = $row->pomoc;
		}
	}

	/*
	 * Adds dogodek for $this Naloga into db.
	 */
	public function dogodek_add($array){
		global $link;
		$cols = ["id_ekipa", "id_naloga"];
		$vals = ["?", $this->id];
		$id_ekipa = $array['id_ekipa'];
		$params = ["stmt", "i", &$id_ekipa];
		switch($this->type){
			case "naloga":
				$cols[] = "vrednost";
				$vals[] = "?";
				$params[1] .= "i";
				$params[] = &$array["naloga_$this->id"];
				$this->datetime = $array["d_zacetek"] . " " . $array["t_zacetek"];
				break;
			case "KT":
				$cols[] = "vrednost";
				$vals[] = "1";
				$this->datetime = $array["d_prihod"] . " " . $array["t_prihod"];
				break;
			case "hitrostna_start":
				$this->datetime = $array["d_odhod"] . " " . $array["t_odhod"];
				break;
			case "hitrostna_cilj":
				$this->datetime = $array["d_prihod"] . " " . $array["t_prihod"];
				break;
			case "cilj":
				$this->datetime = $array["d_prihod"] . " " . $array["t_prihod"];
				break;
		}
		$cols[] = "cas";
		$vals[] = "?";
		$params[1] .= "s";
		$params[] = &$this->datetime;
		if($this->moznost_pomoci){
			$cols[] = "pomoc";
			$vals[] = "'" . ($array["naloga_{$this->id}_pomoc"] == "1" ? "1" : "0") . "'";
		}
		$q = "INSERT INTO dogodek\n\t(" . implode(", ", $cols) . ")\n\tVALUES\n\t(" . implode(", ", $vals) . ");\n";
		if(!$params[0] = mysqli_prepare($link, $q)){
			print("Error preparing q: [\n$q]!\n");
			print_r($params);
			return false;
		}
		if(!call_user_func_array("mysqli_stmt_bind_param", $params)){
			print("Error binding params!\n");
			print("MySQL error: {$params[0]->error}\n");
			return false;
		}
		if(!mysqli_stmt_execute($params[0])){
			print("Error executing statement: [\n$q]!\n");
			print("MySQL error: {$params[0]->error}\n");
			print_r($params);
			return false;
		}
		mysqli_stmt_close($params[0]);
		return true;
	}

	/*
	 * Updates dogodek for $this Naloga in db.
	 */
	public function update_dogodek($array){
		global $link;
		$set = [];
		$params = ["stmt", ""];
		$id_ekipa = $array['id_ekipa'];
		if($this->type == "naloga" && array_key_exists("naloga_$this->id", $array) &&
				(!array_key_exists($id_ekipa, $this->vrednost) || $this->vrednost[$id_ekipa] != $array["naloga_$this->id"])){
			$set[] = "dogodek.vrednost = ?";
			$params[1] .= "i";
			$params[] = &$array["naloga_$this->id"];
		}
		if($this->moznost_pomoci && (!array_key_exists($id_ekipa, $this->pomoc) ||
			$this->pomoc[$id_ekipa] != ($array["naloga_{$this->id}_pomoc"] == "1" ? 1 : 0))){
			$set[] = "dogodek.pomoc = " . ($array["naloga_{$this->id}_pomoc"] == "1" ? "1" : "0");
		}
		foreach(["in" => "prihod", "mid" => "zacetek", "out" => "odhod"] as $b_name => $name){
			if( ($this->id == $this->par->$b_name) &&
				(!array_key_exists($id_ekipa, $this->date) || !array_key_exists($id_ekipa, $this->time) ||
				$this->date[$id_ekipa] != $array["d_$name"] || $this->time[$id_ekipa] != $array["t_$name"])
			){
				$this->dttm[$id_ekipa] = "";
				if(!array_key_exists("d_$name", $array)) $this->dttm[$id_ekipa] .= $this->date[$id_ekipa];
				else $this->dttm[$id_ekipa] .= $array["d_$name"];
				$this->dttm[$id_ekipa] .= " ";
				if(!array_key_exists("t_$name", $array)) $this->dttm[$id_ekipa] .= $this->time[$id_ekipa];
				else $this->dttm[$id_ekipa] .= $array["t_$name"];
				$set[] = "dogodek.cas = ?";
				$params[1] .= "s";
				$params[] = &$this->dttm[$id_ekipa];
			}
		}
		$q = "UPDATE dogodek SET " . implode(", ", $set) . "\n" .
			"\tWHERE (dogodek.id_naloga = $this->id AND dogodek.id_ekipa = ?);\n";
		$params[1] .= "i";
		$params[] = &$id_ekipa;
		if(!$params[0] = mysqli_prepare($link, $q)){
			print("Error preparing query [\n$q]!\n");
			return false;
		}
		if(!call_user_func_array("mysqli_stmt_bind_param", $params)){
			print("Error binding parameters!\n");
			print("MySQL error: {$params[0]->error}\n");
			return false;
		}
		if(!mysqli_stmt_execute($params[0])){
			print("Error executing statement!\n");
			print("MySQL error: {$params[0]->error}\n");
			return false;
		}
		mysqli_stmt_close($params[0]);
		return true;
	}

	/*
	 * Adds (js) Editcol definitions for this Naloga.
	 */
	public function add_Edit_cols(&$Edit_cols){
		if($this->type == "naloga"){
			$Edit_cols[] = "new Editcol({name: 'naloga_$this->id', display_name: '$this->ime', type: 'number', low: 0, high: " . $this->max_tocke/$this->faktor .
				", editable: true, default_value: 0})";
			if($this->moznost_pomoci)
				$Edit_cols[] = "new Editcol({name: 'naloga_{$this->id}_pomoc', display_name: 'pomoč $this->ime', type: 'checkbox', editable: true, default: false})";
		}
	}

	/*
	 * Adds (js) Cell definitions for the datetime columns.
	 */
	public function get_datetime_Cells($id_ekipa, &$cid){
		$Cells[] = "new Cell($id_ekipa, $cid, '" . $this->date[$id_ekipa] . "')";
		++$cid;
		$Cells[] = "new Cell($id_ekipa, $cid, '" . $this->time[$id_ekipa] . "')";
		++$cid;
		return implode(", ", $Cells);
	}

	/*
	 * Adds (js) Cell definitions needed by $this Naloga.
	 */
	public function add_Table_Cells(&$Table_Cells, $id_ekipa, &$cid){
		if($this->type == "naloga"){
			$Table_Cells[] = "new Cell($id_ekipa, $cid, {$this->vrednost[$id_ekipa]})";
			++$cid;
		}
		if($this->moznost_pomoci){
			$Table_Cells[] = "new Cell($id_ekipa, $cid, " . btos($this->pomoc[$id_ekipa]) . ")";
			++$cid;
		}
	}

}

?>
