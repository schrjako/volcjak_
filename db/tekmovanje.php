<?php

require_once("./db/constants.php");
require_once("./db/Table.php");

$tables["tekmovanje"] = new Table(
	"tekmovanje",
	[
		"id" =>
			new Member("id", "INT", flags: "NOT NULL", auto_increment: true),
		"leto" =>
			new Member("leto", "INT"),
		"ime" =>
			new Member("ime", "VARCHAR($slen[1])")
	],
	new Perms("tekmovanje", "id",
		[
			"tekmovanje" => new Perm("tekmovanje", ["leto", "ime"])
		]
	)
);

?>
