<?php

chdir("..");

require_once("./db/connect.php");
require_once("./db/Kontrola_mult.php");

print_r($_POST);

[$KT, $proga] = explode(";", $_POST['t_name']);

$kontrola = new Kontrola_mult($_POST['key'], $_POST['viewkey'], $KT, $proga);

if(!$kontrola->rm_kontrola($KT, $proga, $_POST)){
	header("HTTP/1.0 400 Bad Request");
	exit("Error while removing kontrola!\n");
}

print("Successfully removed kontrola for ekipa {$_POST['id_ekipa']}!\n");

?>
