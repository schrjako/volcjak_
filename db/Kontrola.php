<?php

require_once("./db/utils.php");
require_once("./db/connect.php");
require_once("./db/Key.php");
require_once("./db/Naloga.php");

/*
 * Can create js Table sturctures for user friendly editing of table dogodek.
 */
class Kontrola{

	/*
	 * Holds Kontrola information for one kontrola (combination of KT and proga).
	 * $parent is a Kontrola_mult object.
	 */
	public function __construct($KT, $proga, &$parent){
		$this->KT = $KT;
		$this->proga = $proga;
		$this->par = &$parent;
		$this->kid = "$KT;$proga";
		$this->zacetek = false;
		//$this->konec = false;
		$this->odhod = false;
		$this->rlen = 0;
		$this->existing_ekipa = [];
	}

	/*
	 * Gets all ekipa which are connected to $this->proga through a start.
	 */
	private function get_ekipa(){
		if($this->got_ekipa) return true;
		global $link;
		$q = "SELECT ekipa.* FROM ekipa\n\tJOIN (proga, start)\n\tON (start.id_ekipa = ekipa.id AND start.id_proga = proga.id)\n" .
			"\tWHERE proga.id = '$this->proga';\n";
		$ans = mysqli_query($link, $q);
		while($row = mysqli_fetch_object($ans)){
			$this->ekipa[$row->id] = $row->stevilka . " - " . $row->ime;
		}
		return $this->got_ekipa = true;
	}

	/*
	 * Gets all Naloga objects that describe this Kontrola.
	 */
	private function get_naloga(){
		if($this->got_naloga) return true;
		if(!$this->get_ekipa()) exit("Error getting ekipa!\n");
		$this->clen = 1;	//	id_ekipa, prihodx2(dt)
		global $link;
		$q = "SELECT naloga.* FROM naloga WHERE ('$this->KT' = naloga.id_KT AND '$this->proga' = naloga.id_proga);\n";
		$ans = mysqli_query($link, $q);
		while($row = mysqli_fetch_object($ans)){
			if($row->type == "cilj") $this->clen+=2;
			if($row->type == "KT"){
				if(!$this->in) $this->clen+=2;
				$this->in = true;
			}
			if($row->type == "hitrostna_start"){
				if(!$this->odhod) $this->clen+=2;
				$this->odhod = true;
			}
			if($row->type == "naloga"){
				if(!$this->zacetek) $this->clen+=2;
				$this->zacetek = true;
				++$this->clen;
			}
			//if($this->zacetek && $this->odhod){
			//	if(!$this->konec) $this->clen+=2;
			//	$this->konec = true;
			//}
			$this->naloga[$row->id] = new Naloga($row, $this->clen, $this);
		}
		return $this->got_naloga = true;
	}

	/*
	 * Gets data from db that's relevant for this Kontrola.
	 *
	 * if $id_ekipa is specified, the data retrieved is all for the given ekipa.
	 */
	private function get_data($id_ekipa = -1){
		if($this->got_data) return true;
		if(!$this->get_naloga()){
			print("Error getting cols!\n");
			return false;
		}
		global $link;
		$q = "SELECT dogodek.* FROM dogodek\n\tJOIN naloga\n\tON (dogodek.id_naloga = naloga.id)\n" .
			"\tWHERE (naloga.id_KT = $this->KT AND naloga.id_proga = $this->proga";
		if($id_ekipa != -1){
			$q .= " AND dogodek.id_ekipa = ?";
		}
		$q .= ");\n";
		if(!$stmt = mysqli_prepare($link, $q)){
			print("Error prepareing q: [\n$q]!\n");
			return false;
		}
		if($id_ekipa != -1){
			if(!mysqli_stmt_bind_param($stmt, "i", $id_ekipa)){
				print("Error binding parameters!\n");
				return false;
			}
		}
		if(!mysqli_stmt_execute($stmt)){
			print("Error executing statement!\n");
			return false;
		}
		if(!$result = mysqli_stmt_get_result($stmt)){
			print("Error getting result!\n");
			return false;
		}
		while($row = mysqli_fetch_object($result)){
			$this->naloga[$row->id_naloga]->add_data($row);
			$this->existing_ekipa[$row->id_ekipa] = $row->id_ekipa;
		}
		$this->rlen = count($this->existing_ekipa);
		return $this->got_data = true;
	}

	/*
	 * Adds mrtvi cas for each ekipa that visited this kontrola.
	 */
	public function add_mrtvi_cas(){
		if(!$this->get_data()){
			print("Error getting data for ($this->KT, $this->proga)!\n");
			return false;
		}
		foreach(array_keys($this->par->cas) as $id_ekipa){
			if($this->in && $this->mid){
				if(isset($this->naloga[$this->in]->dttm[$id_ekipa]) && isset($this->naloga[$this->mid]->dttm[$id_ekipa])){
					$this->par->mrtvi_cas[$id_ekipa]->add(
						(new DateTime($this->naloga[$this->in]->dttm[$id_ekipa]))->diff(
							new DateTime($this->naloga[$this->mid]->dttm[$id_ekipa])
						)
					);
				}
			}
			else if($this->in && $this->out){
				if(isset($this->naloga[$this->in]->dttm[$id_ekipa]) && isset($this->naloga[$this->out]->dttm[$id_ekipa])){
					$this->par->mrtvi_cas[$id_ekipa]->add(
						(new DateTime($this->naloga[$this->in]->dttm[$id_ekipa]))->diff(
							new DateTime($this->naloga[$this->out]->dttm[$id_ekipa])
						)
					);
				}
			}
		}
	}

	/*
	 * Adds $this Kontrola into db according to the data in $array.
	 *
	 * If any of the dogodek_add calls fails, all changes are rolled back.
	 */
	public function kontrola_add($array){
		if(!$this->get_naloga()){
			print("Error getting cols!\n");
			return false;
		}
		if(!array_key_exists("id_ekipa", $array)){
			print("Eror: id_ekipa not specified!\n");
			return false;
		}
		global $link;
		mysqli_begin_transaction($link);
		foreach($this->naloga as $id_naloga => $naloga){
			if(!$naloga->dogodek_add($array)){
				mysqli_rollback($link);
				print("rolling back\n");
				return false;
			}
		}
		mysqli_commit($link);
		return true;
	}

	/*
	 * Updates $this Kontrola according to the data in $array.
	 */
	public function update_kontrola($array){
		if(!array_key_exists("id_ekipa", $array)){
			print("Eror: id_ekipa not specified!\n");
			return false;
		}
		if(!$this->get_data($array['id_ekipa'])){
			print("Eror while getting data!\n");
			return false;
		}
		$ret = true;
		foreach($this->naloga as $id_naloga => $naloga){
			if(!$naloga->update_dogodek($array)) $ret = false;
		}
		return $ret;
	}

	/*
	 * Removes all dogodek with id_ekipa equal to p_key element of $array.
	 */
	public function rm_kontrola($array){
		global $link;
		$q = "DELETE dogodek.* FROM dogodek\n\tJOIN naloga\n\tON dogodek.id_naloga = naloga.id\n" .
			"\tWHERE (dogodek.id_ekipa = ? AND naloga.id_KT = $this->KT AND naloga.id_proga = $this->proga);\n";
		if(!$stmt = mysqli_prepare($link, $q)){
			print("Error preparing query [\n$q]!\n");
			return false;
		}
		if(!mysqli_stmt_bind_param($stmt, "i", $array['p_key'])){
			print("Error binding parameters!\n");
			print("MySQL error: {$stmt->error}\n");
			return false;
		}
		if(!mysqli_stmt_execute($stmt)){
			print("Error executing statement!\n");
			print("MySQL error: {$stmt->error}\n");
			return false;
		}
		mysqli_stmt_close($stmt);
		return true;
	}

	/*
	 * Returns all ekipa options for dogodek.
	 */
	private function get_id_ekipa_options(){
		$options = [];
		foreach($this->ekipa as $value => $display){
			$options[] = "$value: '$display'";
		}
		$beg = "{";
		$end = "}";
		return $beg . implode(", ", $options) . $end;
	}

	/*
	 * Returns (js) Editcol definitions for datetime column pair with name $name.
	 */
	private function get_Editcol_datetime($name, $display_name, $indent = ""){
		$editcol  = "new Editcol({name: 'd_$name', display_name: '$display_name datum', type: 'date', editable: true, default_value: ':date'}),\n"
			. "{$indent}new Editcol({name: 't_$name', display_name: '$display_name ura', type: 'time', editable: true, default_value: ':time'})";
		return $editcol;
	}

	/*
	 * Returns (js) Edit object definition for this Kontrola.
	 */
	private function get_Edit_obj($indent = ""){
		$this->get_naloga();
		$Edit_cols[] = "new Editcol({name: 'id_ekipa', display_name: 'ekipa', type: 'select', options: " .
			$this->get_id_ekipa_options() . ", editable: false, allsend: true})";
		foreach($this->naloga as $naloga){
			$naloga->add_Edit_cols($Edit_cols);
		}
		if($this->in) $Edit_cols[] = $this->get_Editcol_datetime("prihod", "prihod", indent: $indent . "\t");
		if($this->zacetek) $Edit_cols[] = $this->get_Editcol_datetime("zacetek", "začetek", indent: $indent . "\t");
		//if($this->konec) $Edit_cols[] = $this->get_Editcol_datetime("konec", "konec", indent: $indent . "\t");
		if($this->odhod) $Edit_cols[] = $this->get_Editcol_datetime("odhod", "odhod", indent: $indent . "\t");
		return "{$indent}edit_obj: new Edit([\n{$indent}\t" . implode(",\n{$indent}\t", $Edit_cols) . "\n{$indent}])";
	}

	/*
	 * Returns js Row definition.
	 * For further explanation look in 'js/Row.js'.
	 */
	private function get_Table_Row($rid, $id_ekipa){
		$Table_Cells = [];
		$Table_Cells[] = "new Cell($id_ekipa, 0, $id_ekipa)";
		$cid = 1;
		foreach($this->naloga as $naloga){
			$naloga->add_Table_Cells($Table_Cells, $id_ekipa, $cid);
		}
		if($this->in)
			$Table_Cells[] = $this->naloga[$this->in]->get_datetime_Cells($id_ekipa, $cid);
		if($this->zacetek)
			$Table_Cells[] = $this->naloga[$this->mid]->get_datetime_Cells($id_ekipa, $cid);
		if($this->odhod)
			$Table_Cells[] = $this->naloga[$this->out]->get_datetime_Cells($id_ekipa, $cid);
		//$p_key = $this->get_p_key_val($rid);
		$Row = "new Row($rid, $id_ekipa, $this->clen, [" .  implode(", ", $Table_Cells) . "])";
		//if($this->permits("hash")) $row .= ", '" . $this->id_to_hash($p_key) . "'";
		return $Row;
	}

	/*
	 * Returns js Rows definitions - object of Row objects.
	 */
	private function get_Table_Rows($indent = ""){
		$this->get_data();
		$Table_Rows = [];
		$r = 0;
		foreach($this->existing_ekipa as $id_ekipa){
			$Table_Rows[] = "$r: " . $this->get_Table_Row($r, $id_ekipa);
			++$r;
		}
		if(count($Table_Rows)!=0) return "{$indent}rows: {\n{$indent}\t" . implode(",\n{$indent}\t", $Table_Rows) . "\n{$indent}}";
		return "{$indent}rows: {}";
	}

	/*
	 * Returns js Table object definition according to the primary keys $KT and $proga.
	 */
	public function get_Table_def($indent = ""){
		if(!$this->get_data()) exit("Error getting data!\n");
		$Table_def = "{$indent}Table_objs['$this->kid'] = new Table({tid: '$this->kid', rlen: $this->rlen, clen: $this->clen, " .
			"key: '" . $this->par->key->get_key() . "', viewkey: '". $this->par->viewkey->get_key() . "', add: true, del: true, " .
			"srvr: 'db/kontrola.php',\n";
		$Table_def .= $this->get_Edit_obj(indent: $indent . "\t") . ",\n";
		$Table_def .= $this->get_Table_Rows(indent: $indent . "\t") . "\n{$indent}});\n";
		return $Table_def;
	}

}

?>
