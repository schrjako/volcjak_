<?php

/*
 * Class holding permissions for a specific key.
 */
class Perm{

	/*
	 * Holds permissions for a table which apply if user has acces to table $name.
	 * $cols records the columns that may be edited, whereas $add, $del, $hash, $view record
	 * the permissions to add or delete rows, get the hash key for displayed rows or just view
	 * the row respectively.
	 */
	public function __construct($name, $cols, $add = false, $del = false, $hash = false,
		$view = false){
		$this->name = $name;
		$this->cols = $cols;
		$this->add = $add;
		$this->del = $del;
		$this->hash = $hash;
		$this->view = $view;
	}

	/*
	 * Returns true or false, depending on the permission at hand.
	 */
	public function permits($permission){
		if($permission == "add" || $permission == "del" || $permission == "hash" ||
				$permission == "view")
			return $this->$permission;
		return in_array($permission, $this->cols);
	}

}

?>
