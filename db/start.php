<?php

require_once("./db/constants.php");
require_once("./db/Table.php");

$tables["start"] = new Table(
	"start",
	[
		"id_ekipa" =>
			new Member("id_ekipa", "INT", display_name: "ekipa", ft_name: "ekipa"),
		"id_proga" =>
			new Member("id_proga", "INT", display_name: "proga", ft_name: "proga"),
		"cas" =>
			new Member("cas", "DATETIME", display_name: "čas")
	],
	new Perms("proga", "id_proga",
		[
			"proga" => new Perm("proga", ["cas"], add: true, del: true, hash: true),
			"ekipa" => new Perm("ekipa", [], view: true)
		]
	),
	p_key: "id_ekipa, id_proga"
);

?>
