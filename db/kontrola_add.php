<?php

chdir("..");

require_once("./db/connect.php");
require_once("./db/Kontrola_mult.php");

[$KT, $proga] = explode(";", $_POST['t_name']);

$kontrola = new Kontrola_mult($_POST['key'], $_POST['viewkey'], $KT, $proga);

if(!$kontrola->kontrola_add($KT, $proga, $_POST)){
	header("HTTP/1.0 400 Bad Request");
	exit("Error while adding kontrola!\n");
}

print("{$_POST['id_ekipa']};\n");

print("Successfully added kontrola for ekipa {$_POST['id_ekipa']}!\n");

?>
