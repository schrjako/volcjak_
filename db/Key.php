<?php

require_once("./db/constants.php");
require_once("./db/connect.php");
require_once("./db/utils.php");

/*
 * Holds information of the user's key.
 */
class Key{

	/*
	 * Creates the Key, checks table and hash correctness. On success it sets
	 * the property 'id' to the correspodning id from db and the property valid
	 * to true, otherwise false.
	 *
	 * $key has to be of the format table_name:hash where table_name represents the
	 * table to whose parent column name (p_c_name, Perms) the hash value fits.
	 * Usually parent column name is id.
	 *
	 * The hash is calculated as sha1(table_name+(a*t.c*t.c+b*t.c+c)) where table_name
	 * represents the table, a, b, and c are the secret and t.c is the value of parent
	 * column name in table table_name.
	 *
	 * If $view is set to true the key hash is calculated with "_view" (without the
	 * quotation marks) inserted in between the table name and value from the quadratic
	 * equasion with global $a, $b, and $c and the table's id before calculating the hash
	 * value (sha1(table_name+'_view'+(a*t.c*t.c+b*t.c+c))). This type of key should only
	 * be used to determine which options for editing exist, not give permission for
	 * the editing itself.
	 */
	public function __construct($key, $view = false){
		global $hash_len, $tables;
		sscanf($key, "%[a-zA-Z]:%[a-f0-9]", $this->t_name, $this->hash);
		if(strlen($this->hash) != $hash_len){
			$this->valid = false;
			exit("Wrong hash length: is " . strlen($this->hash) . ", should be $hash_len!<br>\n");
		}
		if(!array_key_exists($this->t_name, $tables)){
			exit("The key's table ($this->t_name) doesn't seem to exist, check your link!<br>\n");
		}
		global $link, $a, $b, $c;
		if($view) $t_name = $this->t_name . "_view";
		else $t_name = $this->t_name;
		$this->hash_expr = "sha1(CONCAT('$t_name', $a*id*id+$b*id+$c))";
		$q = "SELECT id FROM $this->t_name WHERE $this->hash_expr = '$this->hash';";
		$ans = mysqli_query($link, $q);
		if($ans->num_rows == 0){
			$this->valid = false;
			exit("Couldn't find id to hash value $this->hash in table $this->t_name!<br>\n");
		}
		$row = mysqli_fetch_object($ans);
		if(!property_exists($row, 'id')){
			$this->valid = false;
			exit("Query answer doesn't contain id!<br>\n");
		}
		$this->id = $row->id;
		$this->valid = true;
	}

	/*
	 * Returns the key that can be passed to the Key constructor to create
	 * the given Key.
	 */
	public function get_key(){
		return $this->t_name . ":" . $this->hash;
	}

	/*
	 * Returns MySQL WHERE statement which in conjunction with
	 * Key::root_cross_join defines the key's scope.
	 */
	public function get_where(&$where){
		$s_where = "$this->t_name.id = '$this->id'";
		return $where[$s_where] = $s_where;
	}

	/*
	 * Same as get_join_on (utils.php) but for this instance of Key.
	 */
	public function root_cross_join(&$join, &$on){
		$join[$this->t_name] = $this->t_name;
		$s_on = "";
		get_join_on($this->t_name, $join, $on);
	}

}

?>
