<?php

chdir("..");

require_once("./db/connect.php");
require_once("./db/tables.php");

print_r($_POST);

$t_name = $_POST['t_name'];

if(!array_key_exists($t_name, $tables)){
	header("HTTP/1.0 400 Bad Request");
	exit("Table name is incorrect, table '$t_name' doesn't exist!\n");
}

$table = $tables[$t_name];
$table->set_key($_POST['key']);
if(!isset($table->key) || $table->key->valid == false){
	header("HTTP/1.0 400 Bad Request");
	exit("The key {$_POST['key']} doesn't seem to be valid, check the correctness of your link!\n");
}

if(!array_key_exists($table->key->t_name, $table->perms->perms) || !$table->perms->perms[$table->key->t_name]->del){
	header("HTTP/1.0 400 Bad Request");
	exit("Key seems to be valid, but doesn't have the right permissions in table $table->name!\n");
}

if(!array_key_exists("p_key", $_POST)){
	header("HTTP/1.0 400 Bad Request");
	exit("PRIMARY KEY (p_key) missing from delete request!\n");
}

if(!$table->row_delete($_POST['p_key'])){
	header("HTTP/1.0 400 Bad Request");
	exit("Error deleting row with PRIMARY KEY {$_POST['p_key']}!\n");
}

print("Success!");

?>
