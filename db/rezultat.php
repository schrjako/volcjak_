<?php

require_once("./db/constants.php");
require_once("./db/Table.php");

$tables["rezultat"] = new Table(
	"rezultat",
	[
//		"id" =>
//			new Member("id", "INT", flags: "NOT NULL", auto_increment: true),
		"id_ekipa" =>
			new Member("id_ekipa", "INT", display_name: "ekipa", ft_name: "ekipa"),
		"id_naloga" =>
			new Member("id_naloga", "INT", display_name: "naloga", ft_name: "naloga"),
		"tocke" =>
			new Member("tocke", "INT", display_name: "točke", def: "0")
	],
	new Perms("php", "php",
		[
		]
	),
	p_key: "id_ekipa, id_naloga"
);

?>
