<?php

/*
 * Class which holds the definition of a table column/member.
 */
class Member{

	/*
	 * $name is the name of the column.
	 * $type denotes its data type.
	 * $ft_name the FOREIGN TABLE the member is a FOREIGN KEY to.
	 * $flags enable adding AUTO_INCREMENT and the sort.
	 * $options defines the possible values for type ENUM.
	 * $prefix defines the prefix to be used when just ft_name would be ambiguous
	 * PRIMARY KEY is managed in class Table, due to the possibility
	 * of one with multiple Members.
	 */
	public function __construct($name, $type, $display_name = "", $ft_name = null, $flags = "NOT NULL", $options = [],
		$prefix = "", $auto_increment = false, $def = null){
		$this->name = $name;
		$this->type = $type;
		$this->display_name = $display_name == "" ? $name : $display_name;
		$this->ft_name = $ft_name;
		$this->flags = $flags;
		$this->options = $options;
		$this->prefix = $prefix;
		$this->auto_increment = $auto_increment;
		$this->def = $def;
	}

	/*
	 * Returns the char used by MySQL prepared statements to identify variable type.
	 */
	public function get_MySQL_type_char(){
		switch($this->type){
			case "INT": return "i";
			case "BOOL": return "i";
			case "FLOAT": return "d";
			default: return "s";
		}
	}

	/*
	 * Returns the SQL type of Member.
	 */
	private function get_SQL_type(){
		$SQL_type = "";
		if($this->type == "ENUM"){
			foreach($this->options as $option){
				if($SQL_type == "") $SQL_type = "ENUM(";
				else $SQL_type .= ", ";
				$SQL_type .= "'$option'";
			}
			$SQL_type .= ")";
		}
		else
			$SQL_type = $this->type;
		return $SQL_type;
	}

	/*
	 * Returns SQL DEFAULT value if it is set.
	 */
	private function get_SQL_def(){
		if($this->def != null) return " DEFAULT $this->def";
		return "";
	}

	/*
	 * Returns the create definition for given column
	 */
	public function get_create_definition(){
		$create_definition = "$this->name " . $this->get_SQL_type() . $this->get_SQL_def() .
			($this->auto_increment ? " AUTO_INCREMENT" : "") . " $this->flags";
		if($this->ft_name != null)
			$create_definition .= ", FOREIGN KEY ($this->name) REFERENCES $this->ft_name(id)";
		return $create_definition;
	}

	/*
	 * Adds tables for JOIN and ON statements that connect FOREIGN KEY's. If $prefix is set the table
	 * gets a new alias as it means the possibility of a JOIN with itself.
	 */
	public function root_cross_join(&$join, &$on){
		get_join_on($this->ft_name, $join, $on, prefix: $this->prefix);
		$full_name = $this->prefix . $this->ft_name;
		if($this->prefix != "") $s_join = "$this->ft_name AS $full_name";
		else $s_join = $this->ft_name;
		$join[$s_join] = $s_join;
	}

	/*
	 * Params is the array used in conjunction with call_user_func_array to bind parameters to MySQL
	 * prepared statement (stmt) and where the statement which defines given FK.
	 */
	public function get_param_where(&$params, &$where){
		$full_name = $this->prefix . $this->ft_name;
		$where[] = "$full_name.id = ?";
		$params[1] .= "i";
		$params[] = &$this->values[0];
	}


	/*
	 * Gets the values for FOREIGN KEYs which are displayed to the user.
	 * All input variables are already escaped and are safe to use in a query.
	 */
	public function get_display_values($key){
		global $link, $tables;
		$key->root_cross_join($join, $on);
		get_join_on($this->ft_name, $join, $on);
		if(array_key_exists($key->t_name, $tables[$this->ft_name]->perms->perms))
			$key->get_where($where);
		unset($join[$this->ft_name]);
		$q = "SELECT $this->ft_name.id, $this->ft_name.ime FROM $this->ft_name\n";
		if($join != null) $q .= "\tJOIN (" . implode(", ", $join) . ")\n";
		if($on != null) $q .= "\tON (" . implode(" AND ", $on) . ")\n";
		if($where != null) $q .= "\tWHERE (" . implode(" AND ", $where) . ");\n";
		$ans = mysqli_query($link, $q);
		while($row = mysqli_fetch_object($ans)){
			$this->display_values[$row->id] = $row->ime;
		}
	}

	/*
	 * Returns a list or an object of options.
	 *
	 * If Member is a FOREIGN KEY it returns an object with pairs of (id: display_name)
	 * where id is the FOREIGN KEY value and display_name the name displayed for the given
	 * value for a user friendly UI.
	 *
	 * If the type of Member is ENUM it returns the possible values in an array.
	 */
	private function get_options(){
		$options = [];
		if($this->ft_name != null){
			foreach($this->display_values as $value => $display){
				$options[] = "$value: '$display'";
			}
		}
		else if($this->type == "ENUM"){
			foreach($this->options as $display_name => $option){
				$options[] = "$option: '$display_name'";
			}
		}
		return "{" . implode(", ", $options) . "}";
	}

	/*
	 * Returns the description of the Member.
	 * For further explanation look in 'js/Edit.js'.
	 */
	public function get_edit_desc($editable, $is_p_fk = false, $indent = ""){
		$edit = btos($editable);
		$edit_desc = "{name: '$this->name', display_name: '$this->display_name'";
		if($this->ft_name != null){
			$edit_desc .= ", type: 'select', options: " . $this->get_options();
			if($this->def != null)
				$edit_desc .= ", default_value: '$this->def'";
		}
		else if(strncmp($this->type, "VARCHAR", strlen("VARCHAR")) == 0){
			sscanf($this->type, "VARCHAR(%d)", $len);
			$edit_desc .= ", type: 'text', len: '$len'";
			if($this->def != null)
				$edit_desc .= ", default_value: '$this->def'";
		}
		else if($this->type == "DATETIME"){
			if($this->def != null){
				$edit_desc = "{name: 'd_$this->name', display_name: '$this->display_name datum', type: 'date', allsend: true, " .
					"default_value: ':date', editable: $edit}),\n" .
					"{$indent}new Editcol({name: 't_$this->name', display_name: '$this->display_name ura', allsend: true, type: 'time', " .
					"default_value: ':time'";
			}
			else{
				$edit_desc = "{name: 'd_$this->name', display_name: '$this->display_name datum', type: 'date', allsend: true, editable: $edit}),\n" .
					"{$indent}new Editcol({name: 't_$this->name', display_name: '$this->display_name ura', allsend: true, type: 'time'";
			}
		}
		else{
			switch($this->type){
				case "INT":
					$edit_desc .= ", type: 'number'";
					break;
				case "BOOL":
					$edit_desc .= ", type: 'checkbox'";
					break;
				case "FLOAT":
					$edit_desc .= ", type: 'number', step: 0.01";
					break;
				case "TIME":
					$edit_desc .= ", type: 'time'";
					break;
				case "ENUM":
					$edit_desc .= ", type: 'select', options: " . $this->get_options();
					break;
			}
			if($this->def != null){
				switch($this->type){
					case "INT":
						$edit_desc .= ", default_value: $this->def";
						break;
					case "BOOL":
						$edit_desc .= ", default_value: " . btos($this->def);
						break;
					case "FLOAT":
						$edit_desc .= ", default_value: $this->def";
						break;
					case "TIME":
						$edit_desc .= ", default_value: '$this->def'";
						break;
					case "ENUM":
						$edit_desc .= ", default_value: '$this->def'";
						break;
				}
			}
		}
		if($is_p_fk) $edit_desc .= ", link: true";
		$edit_desc .= ", editable: $edit}";
		return $edit_desc;
	}

	/*
	 * Returns js Cell definition.
	 * For further explanation look in 'js/Cell.js'.
	 */
	public function get_Table_Cell($rid, &$cid){
		/*
		 * DATETIME is split on the date (first 10 characters) and time (last 8 characters).
		 */
		switch($this->type){
			case "DATETIME":
				$Table_Cell = "new Cell($rid, $cid, '" . substr($this->values[$rid], 0, 10) . "'), ";
				++$cid;
				$Table_Cell .= "new Cell($rid, $cid, '" . substr($this->values[$rid], 11, 8) . "')";
				break;
			case "BOOL":
				$Table_Cell = "new Cell($rid, $cid, " . btos($this->values[$rid]) . ")";
				break;
			default:
				$Table_Cell = "new Cell($rid, $cid, '{$this->values[$rid]}')";
				break;
		}
		return $Table_Cell;
	}

}

?>
