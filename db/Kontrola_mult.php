<?php

require_once("./db/tables.php");
require_once("./db/utils.php");
require_once("./db/connect.php");
require_once("./db/Kontrola.php");

/*
 * Handles all Kontrola that fall under $key and $viewkey's domain.
 */
class Kontrola_mult{

	/*
	 * $key is the key that identifies the permissions for given user and is always
	 * either of the table KT or tekmovanje. The dogodek that can be edited all have
	 * id_naloga pointing to a naloga that points to KT (or after that up to tekmovanje)
	 * with its FOREIGN KEYs.
	 *
	 * $viewkey gives permission to read all naloga in its domain for reading and creating,
	 * modifying and deleting dogodek rows. It should always be a proga, kategorija, or
	 * tekmovanje key, that had $view set to true at the creation (for more info look to
	 * db/Key.php.
	 *
	 * sel_proga and sel_KT represent the selected KT and proga (which are used by Kontrola
	 * specific functions, when alternative values aren't provided.
	 */
	public function __construct($key, $viewkey, $KT = -1, $proga = -1){
		$this->key = new Key($key);
		$this->viewkey = new Key($viewkey, true);
		$this->get_display_values($this->key, "KT");
		$this->get_display_values($this->viewkey, "proga");
		if($KT != -1) $this->select("KT", $KT);
		if($proga != -1) $this->select("proga", $proga);
		$this->kontrola = [];
	}

	/*
	 * Selects given value for given name (name can be KT or proga).
	 */
	public function select($name, $value){
		if(!array_key_exists($value, $this->$name)) exit("Error: id $name ($value) is not under given key!<br>\n");
		$full_name = "sel_" . $name;
		$this->$full_name = $value;
	}

	/*
	 * Gets the values for FOREIGN KEYs which are displayed to the user.
	 */
	private function get_display_values(&$key, $t_name){
		global $link;
		$key->root_cross_join($join, $on);
		get_join_on($t_name, $join, $on);
		$key->get_where($where);
		unset($join[$t_name]);
		$q = "SELECT $t_name.id, $t_name.ime FROM $t_name";
		if($join != null) $q .= "\n\tJOIN (" . implode(", ", $join) . ")";
		if($on != null) $q .= "\n\tON (" . implode(" AND ", $on) . ")";
		if($where != null) $q .= "\n\tWHERE (" . implode(" AND ", $where) . ")";
		$q .= ";";
		$ans = mysqli_query($link, $q);
		while($row = mysqli_fetch_object($ans)){
			$this->$t_name[$row->id] = $row->ime;
		}
	}

	/*
	 * Adds a new Kontrola if it does not exist but KT and proga are valid.
	 *
	 * Returns true on success and false on failure.
	 */
	private function add_Kontrola($KT, $proga){
		if(!array_key_exists($KT, $this->KT)){
			print("Error, KT ($KT) does not exist for given key!\n");
			return false;
		}
		if(!array_key_exists($proga, $this->proga)){
			print("Error, proga ($proga) does not exist for given viewkey!\n");
			return false;
		}
		if(!array_key_exists($KT, $this->kontrola) || !array_key_exists($proga, $this->kontrola[$KT]))
			$this->kontrola[$KT][$proga] = new Kontrola($KT, $proga, $this);
		return true;
	}

	/*
	 * Adds kontrola specified in $array to db.
	 */
	public function kontrola_add($KT, $proga, $array){
		if(!$this->add_Kontrola($KT, $proga)) return false;
		return $this->kontrola[$KT][$proga]->kontrola_add($array);
	}

	/*
	 * Updates kontrola in db.
	 */
	public function update_kontrola($KT, $proga, $array){
		if(!$this->add_Kontrola($KT, $proga)) return false;
		return $this->kontrola[$KT][$proga]->update_kontrola($array);
	}

	/*
	 * Removes kontrola from db.
	 */
	public function rm_kontrola($KT, $proga, $array){
		if(!$this->add_Kontrola($KT, $proga)) return false;
		return $this->kontrola[$KT][$proga]->rm_kontrola($array);
	}

	/*
	 * Returns js Table object definition according to the primary keys $KT and $proga.
	 */
	public function get_Table_def($KT = -1, $proga = -1, $tid = "", $indent = ""){
		if($KT = -1) $KT = $this->sel_KT;
		if($proga = -1) $proga = $this->sel_proga;
		if(!array_key_exists($KT, $this->KT) || !array_key_exists($proga, $this->proga))
			return "";
		$this->add_Kontrola($KT, $proga);
		return $this->kontrola[$KT][$proga]->get_Table_def($indent);
	}

	/*
	 * Returns the key or viewkey (key by default) of the selected row if there is one selected
	 * and the original if none is selected.
	 */
	public function get_key($view = false){
		if($view){
			if(isset($this->sel_proga)) return "proga:" . id_to_hash("proga_view", $this->sel_proga);
			else return $this->viewkey->get_key();
		}
		else{
			if(isset($this->sel_KT)) return "KT:" . id_to_hash("KT", $this->sel_KT);
			else return $this->key->get_key();
		}
	}

	/*
	 * Returns the name of specified Kontrola in the format [proga_name: KT_name]: (naloga1 [naloga2 ...]) | (mrtvi KT).
	 */
	public function get_name($KT = -1, $proga = -1){
		if($KT == -1) $KT = $this->sel_KT;
		if($proga == -1) $proga = $this->sel_proga;
		if(!array_key_exists($KT, $this->KT) || !array_key_exists($proga, $this->proga)) return "neobstoječa Kontrola";
		global $link;
		$proga_name = mysqli_fetch_object(mysqli_query($link, "SELECT proga.ime FROM proga WHERE proga.id = $proga"))->ime;
		$KT_name = mysqli_fetch_object(mysqli_query($link, "SELECT KT.ime FROM KT WHERE KT.id = $KT"))->ime;
		$q = "SELECT naloga.ime FROM naloga WHERE (naloga.id_KT = $KT AND naloga.id_proga = $proga AND NOT naloga.type = 'KT');";
		$ans = mysqli_query($link, $q);
		$names = [];
		while($row = mysqli_fetch_object($ans)){
			$names[] = $row->ime;
		}
		return "[$proga_name; $KT_name]: " . (count($names) != 0 ? implode(", ", $names) : "mrtvi KT");
	}

	/*
	 * Returns <div> with links to all valid Kontrola types for given key and viewkey.
	 */
	public function get_links($indent = ""){
		global $link;
		$links = [];
		foreach($this->KT as $id_KT => $KT_name){
			foreach($this->proga as $id_proga => $proga_name){
				$q = "SELECT COUNT(naloga.id) FROM naloga WHERE (naloga.id_KT = $id_KT AND naloga.id_proga = $id_proga);";
				$ans = mysqli_query($link, $q);
				if(mysqli_fetch_object($ans)->{'COUNT(naloga.id)'} != 0){
					$links[] = "<p><a href='kontrola.php?key=" . $this->key->get_key() . "&viewkey=" . $this->viewkey->get_key() .
						"&KT=$id_KT&proga=$id_proga'>" . $this->get_name($id_KT, $id_proga) . " </a></p>";
				}
			}
		}
		if(count($links)<=1) return "";
		return "{$indent}<div>\n{$indent}\t" . implode("\n{$indent}\t", $links) . "\n{$indent}</div>\n";
	}

}

?>
