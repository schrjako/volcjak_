<?php

require_once("./db/constants.php");
require_once("./db/Table.php");

$tables["kategorija"] = new Table(
	"kategorija",
	[
		"id" =>
			new Member("id", "INT", flags: "NOT NULL", auto_increment: true),
		"id_tekm" =>
			new Member("id_tekm", "INT", display_name: "tekmovanje", ft_name: "tekmovanje"),
		"ime" =>
			new Member("ime", "VARCHAR($slen[0])")
	],
	new Perms("tekmovanje", "id_tekm",
		[
			"tekmovanje" => new Perm("tekmovanje", ["ime"], add: true, del: true, hash: true),
			"kategorija" => new Perm("kategorija", ["ime"])
		]
	)
);

?>
