<?php

require_once("./db/constants.php");
require_once("./db/Table.php");

$tables["ekipa"] = new Table(
	"ekipa",
	[
		"id" =>
			new Member("id", "INT", flags: "NOT NULL", auto_increment: true),
		"id_kat" =>
			new Member("id_kat", "INT", display_name: "kategorija", ft_name: "kategorija"),
		"stevilka" =>
			new Member("stevilka", "INT", display_name: "štartna številka"),
		"rod_kratica" =>
			new Member("rod_kratica", "VARCHAR($slen[0])", display_name: "rod"),
		"ime" =>
			new Member("ime", "VARCHAR($slen[1])"),
		"kontakt" =>
			new Member("kontakt", "VARCHAR($slen[1])"),
		"st_clanov" =>
			new Member("st_clanov", "INT", display_name: "št. članov", def: "4"),
		"spremljevalec" =>
			new Member("spremljevalec", "BOOL", def: "false"),
		"v_konkurenci" =>
			new Member("v_konkurenci", "BOOL", display_name: "je v konkurenci", def: "true")
	],
	new Perms("kategorija", "id_kat",
		[
			"kategorija" => new Perm("kategorija", ["stevilka", "rod_kratica", "ime", "kontakt", "st_clanov", "spremljevalec", "v_konkurenci"],
					add: true, del: true, hash: true),
			"ekipa" => new Perm("ekipa", ["rod_kratica", "ime", "kontakt", "st_clanov"])
		]
	)
);

?>
