<?php

require_once("./db/constants.php");
require_once("./db/Table.php");

$tables["proga"] = new Table(
	"proga",
	[
		"id" =>
			new Member("id", "INT", flags: "NOT NULL", auto_increment: true),
		"id_kat" =>
			new Member("id_kat", "INT", display_name: "kategorija", ft_name: "kategorija"),
		"id_KT_start" =>
			new Member("id_KT_start", "INT", display_name: "KT start", ft_name: "KT", prefix: "start_"),
		"id_KT_cilj" =>
			new Member("id_KT_cilj", "INT", display_name: "KT cilj", ft_name: "KT", prefix: "cilj_"),
		"ime" =>
			new Member("ime", "VARCHAR($slen[1])"),
		"casovnica" =>
			new Member("casovnica", "TIME"),
		"odbitek_m" =>
			new Member("odbitek_m", "INT", display_name: "odbitek na minuto", def: "2")
	],
	new Perms("kategorija", "id_kat",
		[
			"kategorija" => new Perm("kategorija", ["id_KT_start", "id_KT_cilj", "ime", "casovnica", "odbitek_m"], add: true, del: true, hash: true),
			"proga" => new Perm("proga", ["id_KT_start", "id_KT_cilj", "ime", "casovnica", "odbitek_m"])
		]
	)
);

?>
