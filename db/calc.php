<?php

chdir("..");

require_once("./db/connect.php");
require_once("./db/constants.php");
require_once("./db/tables.php");
require_once("./db/Tekmovanje.php");

print_r($_POST);

$tekmovanje = new Tekmovanje($_POST['key'], $_POST['table'], (int)$_POST['id']);

if(!$tekmovanje->valid){
	header("HTTP/1.0 400 Bad Request");
	exit("Error creating tekmovanje!\n");
}

if(!$tekmovanje->calc_results()){
	header("HTTP/1.0 400 Bad Request");
	exit("Error calculating results!\n");
}

?>
