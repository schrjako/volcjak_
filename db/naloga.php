<?php

require_once("./db/constants.php");
require_once("./db/Table.php");

$tables["naloga"] = new Table(
	"naloga",
	[
		"id" =>
			new Member("id", "INT", flags: "NOT NULL", auto_increment: true),
		"id_proga" =>
			new Member("id_proga", "INT", display_name: "proga", ft_name: "proga"),
		"id_KT" =>
			new Member("id_KT", "INT", display_name: "KT", ft_name: "KT"),
		"type" =>
			new Member("type", "ENUM", display_name: "tip naloge",
				options: ["naloga" => "naloga", "KT" => "KT", "hitrostna start" => "hitrostna_start", "hitrostna cilj" => "hitrostna_cilj", "cilj" => "cilj"]),
		"ime" =>
			new Member("ime", "VARCHAR($slen[1])"),
		"max_tocke" =>
			new Member("max_tocke", "INT", display_name: "naj. št. točk", def: "100"),
		"faktor" =>
			new Member("faktor", "FLOAT", display_name: "korak/faktor", def: "1"),
		"moznost_pomoci" =>
			new Member("moznost_pomoci", "BOOL", display_name: "možnost pomoči", def: "false")
	],
	new Perms("proga", "id_proga",
		[
			"proga" => new Perm("proga", ["type", "ime", "max_tocke", "faktor", "moznost_pomoci"], add: true, del: true, hash: true),
			"naloga" => new Perm("naloga", ["type", "ime", "max_tocke", "faktor", "moznost_pomoci"])
		]
	)
);

?>
