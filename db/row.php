<?php

if(!array_key_exists("action", $_POST)){
	header("HTTP/1.0 Bad Request");
	exit("Action not specified!\n");
}

$action = $_POST["action"];
unset($_POST["action"]);

switch($action){
	case "add": require("./row_add.php"); break;
	case "mod": require("./row_mod.php"); break;
	case "rm": require("./row_rm.php"); break;
	default: header("HTTP/1.0 Bad Request"); exit("Action [$action] not recognized!\n");
}

?>
