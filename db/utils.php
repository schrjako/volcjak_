<?php

/*
 * Adds all the table-names from (but not including) $t_name to tekmovanje into $join.
 * If $prefix is set, the first table-name gets that prefix (for self-JOINing).
 *
 * Adds statements to $on which enforce the FK constraints.
 *
 * $end_t can be set as a alternative endpoint (instead of the root tekmovanje).
 */
function get_join_on($t_name, &$join, &$on, $end_t = "tekmovanje", $prefix = ""){
	if($t_name == $end_t) return;
	global $tables;
	$table = &$tables[$t_name];
	$join[$table->perms->parent] = $table->perms->parent;
	$s_on .= "$prefix$t_name.{$table->perms->p_c_name} = {$table->perms->parent}.id";
	$on[$s_on] = $s_on;
	get_join_on($table->perms->parent, $join, $on, end_t: $end_t);
}

/*
 * Returns the bool in written form (for printing to javascript.
 */
function btos($bool){
	return $bool?"true":"false";
}

/*
 * Returns the (php) DateInterval in minutes.
 */
function interval_to_min($interval){
	$ret = $interval->d;
	$ret *= 24;
	$ret += $interval->h;
	$ret *= 60;
	$ret += $interval->i;
	if($interval->invert) $ret *= -1;
	return $ret;
}

/*
 * Takes table name (with or without _view) and id and returns the associated hash value.
 */
function id_to_hash($t_name, $id){
	global $a, $b, $c;
	return sha1($t_name . $a*$id*$id+$b*$id+$c);
}

?>
