<?php

require_once("./db/utils.php");
require_once("./db/connect.php");
require_once("./db/Proga.php");

/*
 * Class for calculating results.
 */
class Tekmovanje{

	/*
	 * $key defines the Key and has to be tekmovanje or kategorija, or proga.
	 * $t_name and $id define the scope of this Tekmovanje instance and should be tekmovanje|kategorija|proga
	 * and a valid id for the table $t_name respectively.
	 */
	public function __construct($key, $t_name, $id){
		$this->key = new Key($key);
		$this->valid = false;
		if($this->key->valid == false){
			print("Error: key $key is not valid!\n");
			return;
		}
		if($this->key->t_name != "tekmovanje" && $this->key->t_name != "kategorija" && $this->key->t_name != "proga"){
			print("The key [$key] is for table [{$this->key->t_name}], but should be for tekmovanje, kategorija, or proga!<br>\n");
			return;
		}
		if($t_name != "tekmovanje" && $t_name != "kategorija" && $t_name != "proga"){
			print("Error: table is [$t_name], but should be tekmovanje, kategorija, or proga!\n");
			return;
		}
		$this->t_name = $t_name;
		if((int)$id <= 0){
			print("Id should be a positive integer but is [$id]!<br>\n");
			return;
		}
		$this->id = (int)$id;
		global $link;
		$q = "SELECT DISTINCT tekmovanje.id FROM proga JOIN (kategorija, tekmovanje) " .
			"ON (proga.id_kat = kategorija.id AND kategorija.id_tekm = tekmovanje.id) " .
			"WHERE " . $this->key->get_where($where) . " AND $t_name.id = $id;\n";
		$ans = mysqli_query($link, $q);
		if($ans->num_rows != 1){	//	key and Tekmovanje are from the same scope.
			print("Error: key [$key] and the row in table [$t_name] with the id [$id] aren't of the same competition!\n");
			return;
		}
		$this->valid = true;
		$this->proga = [];
	}

	/*
	 * Calculates the results for the given scope (goes through all the proga-rows in the scope
	 * and calculates each proga's results independently).
	 *
	 * Returns true on success and false on at least 1 proga failing.
	 */
	public function calc_results(){
		global $link;
		$q = "SELECT proga.id, proga.ime\n\tFROM proga\n\tJOIN (kategorija, tekmovanje)\n" .
			"\tON (proga.id_kat = kategorija.id AND kategorija.id_tekm = tekmovanje.id)\n" .
			"\tWHERE " . $this->key->get_where($where) . " AND $this->t_name.id = $this->id;\n";
		$proge = mysqli_query($link, $q);
		$torf = true;
		while($proga = mysqli_fetch_object($proge)){
			$this->proga[$proga->id] = new Proga($this->key, $proga->id);
			if(!$this->proga[$proga->id]->calc_results()){
				print("Error calculating results for proga with id [$proga->id] and name [$proga->ime]!\n");
				$torf = false;
			}
		}
		return $torf;
	}

}

?>
