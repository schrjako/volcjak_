<?php

require_once("./db/constants.php");
require_once("./db/utils.php");
require_once("./db/connect.php");
require_once("./db/Kontrola.php");

/*
 * Class that defines a proga and can calculate results for itself.
 */
class Proga{

	/*
	 * $key is a valid Key object, $id the id of this proga.
	 */
	public function __construct($key, $id){
		$this->key = $key;
		$this->id = (int) $id;
		$this->kontrola = [];
		$this->t0 = new DateTime("00:00");
		$this->cas = [];
		$this->mrtvi_cas = [];
		global $link;
		$q = "SELECT DISTINCT naloga.id_KT AS id FROM naloga WHERE naloga.id_proga = $id;\n";
		$ans = mysqli_query($link, $q);
		while($KT = mysqli_fetch_object($ans)){	//	finds all the KT of this proga.
			$this->kontrola[$KT->id] = new Kontrola($KT->id, $id, $this);
		}
		$q = "SELECT start.id_ekipa AS id, start.cas FROM start WHERE start.id_proga = $id;";
		$ans = mysqli_query($link, $q);
		while($ekipa = mysqli_fetch_object($ans)){	//	finds all ekipa that have a start for this proga.
			$this->cas[$ekipa->id] = new DateTime($ekipa->cas);
			$this->mrtvi_cas[$ekipa->id] = clone $this->t0;
		}
	}

	/*
	 * Calculates casovnica of this proga for each ekipa.
	 */
	public function calc_casovnica(){
		foreach($this->kontrola as $id_KT => $kontrola){
			$kontrola->add_mrtvi_cas();
		}
		global $link;
		$torf = true;
		foreach(array_keys($this->cas) as $id_ekipa){
			if(!isset($this->cilj->dttm[$id_ekipa])) continue;
			$int1 = $this->t0->diff($this->mrtvi_cas[$id_ekipa]);
			$int2 = $this->cas[$id_ekipa]->diff(new DateTime($this->cilj->dttm[$id_ekipa]));
			$mrtvi_cas = interval_to_min($this->t0->diff($this->mrtvi_cas[$id_ekipa]));
			$cas_na_progi = interval_to_min($this->cas[$id_ekipa]->diff(new DateTime($this->cilj->dttm[$id_ekipa])));
			$porabljen_cas = $cas_na_progi - $mrtvi_cas;
			if($porabljen_cas > 2 * $this->cilj->max_tocke){
				$q = "UPDATE ekipa SET ekipa.v_konkurenci = false WHERE ekipa.id = $id_ekipa;\n";
				if(!mysqli_query($link, $q)){
					print("Error throwing ekipa $id_ekipa out for exceeding the time limit!\n");
					print(mysqli_error($link) . "\n");
					print("$q\n");
					return false;
				}
			}
			$tocke = max($porabljen_cas - $this->cilj->max_tocke, 0) * $this->cilj->faktor;
			$q = "INSERT INTO rezultat (id_ekipa, id_naloga, tocke) VALUES ($id_ekipa, {$this->cilj->id}, -$tocke)\n" .
				"\tON DUPLICATE KEY UPDATE rezultat.tocke = -$tocke;\n";
			if(!mysqli_query($link, $q)){
				print("Error updating casovnica!\n");
				print(mysqli_error($link) . "\n");
				print("$q\n");
				$torf = false;
			}
		}
		return $torf;
	}

	/*
	 * Calculates the points for all naloga of type 'naloga' for this proga.
	 */
	private function calc_naloga(){
		global $pomoc_faktor, $link;
		$q = "INSERT INTO rezultat (id_ekipa, id_naloga, tocke)\n" .
			"\t\tSELECT dogodek.id_ekipa, dogodek.id_naloga, dogodek.vrednost * naloga.faktor * IF(dogodek.pomoc = 0, 1, $pomoc_faktor) AS tocke\n" .
			"\t\tFROM dogodek\n" .
			"\t\tJOIN naloga\n" .
			"\t\tON dogodek.id_naloga = naloga.id\n" .
			"\t\tWHERE (naloga.id_proga = $this->id AND naloga.type = 'naloga')\n" .
			"\tON DUPLICATE KEY UPDATE rezultat.tocke = dogodek.vrednost * naloga.faktor * IF(dogodek.pomoc = 0, 1, $pomoc_faktor);\n";
		if(!mysqli_query($link, $q)){
			print("Error calculating naloga tocke!\n");
			print(mysqli_error($link) . "\n");
			print("$q\n");
			return false;
		}
		return true;
	}

	/*
	 * Calculates the points for all naloga of type 'KT' for this proga.
	 */
	private function calc_KT(){
		global $pomoc_faktor, $link;
		$q = "INSERT INTO rezultat (id_ekipa, id_naloga, tocke)\n" .
			"\t\tSELECT dogodek.id_ekipa, dogodek.id_naloga, dogodek.vrednost * naloga.faktor * IF(dogodek.pomoc = 0, 1, $pomoc_faktor) AS tocke\n" .
			"\t\tFROM dogodek\n" .
			"\t\tJOIN naloga\n" .
			"\t\tON dogodek.id_naloga = naloga.id\n" .
			"\t\tWHERE (naloga.id_proga = $this->id AND naloga.type = 'KT')\n" .
			"\tON DUPLICATE KEY UPDATE rezultat.tocke = dogodek.vrednost * naloga.faktor * IF(dogodek.pomoc = 0, 1, $pomoc_faktor);\n";
		if(!mysqli_query($link, $q)){
			print("Error calculating KT tocke!\n");
			print(mysqli_error($link) . "\n");
			print("$q\n");
			return false;
		}
		return true;
	}

	/*
	 * Calculates the points for all naloga of type 'hitrostna_start' and 'hitrostna_cilj' for this proga.
	 */
	private function calc_hitrostna(){
		global $link;
		$q = "SELECT n_start.id FROM naloga AS n_start\n\tJOIN naloga AS n_cilj\n" .
			"\tON (n_start.id = n_cilj.faktor AND n_cilj.id_proga = n_start.id_proga)\n" .
			"\tWHERE (n_start.id_proga = $this->id AND n_start.type = 'hitrostna_start' AND n_cilj.type = 'hitrostna_cilj');\n";
		$hitrostne = mysqli_query($link, $q);
		$torf = true;
		while($hitrostna = mysqli_fetch_object($hitrostne)){
			$q = "SELECT MIN(TO_SECONDS(d_cilj.cas) - TO_SECONDS(d_start.cas))\n" .
				"\tINTO @najcs\n" .
				"\tFROM dogodek AS d_start\n" .
				"\tJOIN (dogodek AS d_cilj, naloga AS n_start, naloga AS n_cilj)\n" .
				"\tON (d_start.id_naloga = n_start.id AND d_cilj.id_naloga = n_cilj.id AND n_cilj.faktor = n_start.id\n" .
				"\t\tAND d_start.id_ekipa = d_cilj.id_ekipa)\n" .
				"\tWHERE (n_start.type = 'hitrostna_start' AND n_cilj.type = 'hitrostna_cilj' AND n_start.id_proga = $this->id AND\n" .
				"\t\tn_cilj.id_proga = $this->id AND n_start.id = {$hitrostna->id});\n" .
				"\n" .
				"INSERT INTO rezultat(id_ekipa, id_naloga, tocke)\n" .
				"\tSELECT d_cilj.id_ekipa, d_cilj.id_naloga,\n" .
				"\t\tGREATEST(0, n_start.faktor - (TO_SECONDS(d_cilj.cas) - TO_SECONDS(d_start.cas)) / @najcs) * n_cilj.max_tocke\n" .
				"\tFROM dogodek AS d_start\n" .
				"\tJOIN (dogodek AS d_cilj, naloga AS n_start, naloga AS n_cilj)\n" .
				"\tON (d_start.id_naloga = n_start.id AND d_cilj.id_naloga = n_cilj.id AND n_cilj.faktor = n_start.id\n" .
				"\t\tAND d_start.id_ekipa = d_cilj.id_ekipa)\n" .
				"\t\tWHERE (n_start.type = 'hitrostna_start' AND n_cilj.type = 'hitrostna_cilj' AND n_start.id_proga = $this->id AND\n" .
				"\t\t\tn_cilj.id_proga = $this->id AND n_start.id = {$hitrostna->id})\n" .
				"\tON DUPLICATE KEY UPDATE rezultat.tocke =\n" .
				"\t\tGREATEST(0, n_start.faktor - (TO_SECONDS(d_cilj.cas) - TO_SECONDS(d_start.cas)) / @najcs) * n_cilj.max_tocke;\n";
			if(!mysqli_multi_query($link, $q)){
				print("Error calculating hitrostna tocke!\n");
				print(mysqli_error($link) . "\n");
				print("$q\n");
				$torf = false;
			}
		}
		return $torf;
	}

	/*
	 * Calculates all the results for this proga.
	 */
	public function calc_results(){
		$torf = true;
		$torf &= $this->calc_casovnica();
		$torf &= $this->calc_naloga();
		$torf &= $this->calc_KT();
		$torf &= $this->calc_hitrostna();
		return $torf;
	}

}

?>
