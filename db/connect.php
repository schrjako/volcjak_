<?php

/*
 * dbconfig.php defines server, username, password and database-name to which
 * dbconnect.php connects, creating the link $link.
 *
 * It also defines factors a b and c, which should be random large ([10^4, 10^5])
 * numbers which prevent reversing the hashes used for authentication.
 */
require("./db/config.php");

$link = mysqli_connect($server, $username, $password);

if(mysqli_connect_errno()){
	print("Connect Failed: ".mysqli_connect_error()."\n");
	exit();
}

mysqli_select_db($link, $dbname);
mysqli_query($link, "set names utf8");

?>
