<?php

require_once("./db/Perm.php");

/*
 * Describes all permissions for a table.
 */
class Perms{

	/*
	 * $parent denotes the parent table - the next higher ranked table and the element
	 * in perms with the most permissions. perms is an array with permissions, each
	 * element describing the permissions for a user with access to given table.
	 * $p_c_name points to the column that is a FOREIGN key to parent table.
	 */
	public function __construct($parent, $p_c_name, $perms){
		$this->parent = $parent;
		$this->p_c_name = $p_c_name;	//	parent column name
		$this->perms = $perms;
	}

	/*
	 * Adds a parent with all the permissions the original parent has ($this->parent).
	 */
	public function add_parent($t_name){
		$this->perms[$t_name] = &$this->perms[$this->parent];
	}

	/*
	 * Returns whether $key allows the user to edit $col_name.
	 */
	public function permits($key, $col_name){
		return $this->perms[$key->t_name]->permits($col_name);
	}

}

?>
